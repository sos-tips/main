10% of data, at least one instance of each code in Sz1
one line = one patch

We are aiming for 80% agreement or above.

citation for 10-20% of data for IRR:
Eagan, B.R., Rogers, B., Serlin, R., Ruis, A.R., Irgens, G.A., Shaffer, D.W.: Can we rely on IRR? testing the assumptions of inter-rater reliability. In: CSCL 2017 Proceedings, pp. 529–532 (2017)