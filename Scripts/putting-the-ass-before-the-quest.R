### Set paths to use later on
basePath <- here::here();
dataPath <- file.path(basePath, "Data");
resultsPath <- file.path(basePath, "results");

devtools::load_all("B:/git/R/rock");

dat <-
  rock::parse_sources(
    path = file.path(dataPath),
    regex = paste0("^\\d{3}_HCI_coded_anchorByPatch.txt$|",
                   "^\\d{3}_SC_coded_anchorByPatch.txt$|",
                   "^\\d{3}_TA_coded_anchorByPatch.rock$|",
                   "^case_attributes.txt$"
                   )
  );

dat2 <-
  rock::sync_streams(
    dat,
    primaryStream = "TA",
    columns = NULL,
    prependStreamIdToColName = FALSE,
    appendStreamIdToColName = TRUE,
    sep = " ",
    fill = TRUE,
    compressFun = NULL,
    #compressFunPart = sum,
    compressFunPart = function(x) return(as.numeric(any(x))),
    expandFun = NULL,
    carryOverAnchors = TRUE,
    colNameGlue = rock::opts$get("colNameGlue"),
    silent = TRUE
  );

sourceIds <-
  unique(dat2$syncResults$qdt$sourceId);
sourceIds <- sourceIds[!is.na(sourceIds)];
sourceIds <- sort(sourceIds);

QUESTs <-
  lapply(
    sourceIds,
    function(sourceId) {
      res <- list();
      res$miniROCKobject <-
        list(
          qdt =
            dat2$syncResults$qdt[dat2$syncResults$qdt$sourceId == sourceId, ]
        );
      class(res$miniROCKobject) <- "rock_parsedSource";
      res$stateTable <-
        rock::get_state_transition_table(
          res$miniROCKobject,
          "state_raw_SC"
        );
      res$stateDf <-
        stateDf_source1 <- rock::get_state_transition_df(
          res$stateTable
        );
      res$stateDotCode <- rock::get_state_transition_dot(
        res$stateDf
      );
      return(res);
    }
  );
names(QUESTs) <- sourceIds;

for (i in sourceIds) {
  writeLines(
    as.character(QUESTs[[i]]$stateDotCode),
    file.path(resultsPath, paste0("QUEST_", i, ".dot"))
  );
  
  writexl::write_xlsx(
    QUESTs[[i]]$stateDf,
    path = file.path(resultsPath, paste0("stateTransitions_", i, ".xlsx"))
  );
  
}


     

writexl::write_xlsx(
  dat2$syncResults$qdt,
  path = file.path(dataPath, "synced_qdt.xlsx")
);

dat3 <- dat2;

dat3$syncResults$qdt <-
  dat3$syncResults$qdt[
    dat3$syncResults$qdt$anchors_persistent != "no_id"
    ,
  ];

### Verify whether the "no_id" rows disappeared
dat3$syncResults$qdt$anchors_persistent[340:360]

allTheCodes <-
  dat3$convenience$codingLeaves;

allTheCodesInAllStreams <-
  c(
    allTheCodes,
    paste0(allTheCodes, "_HCI"),
    paste0(allTheCodes, "_SC")
  );

dat4 <-
  by(
    dat3$syncResults$qdt,
    INDICES = dat3$syncResults$qdt$anchors_persistent,
    FUN = function(anchorPairDf) {
      res <- anchorPairDf[, allTheCodesInAllStreams];
      res <- as.data.frame(t(as.numeric(colSums(res) > 0)));
      names(res) <- allTheCodesInAllStreams;
      res$anchor <- unique(anchorPairDf$anchors_persistent);
      ### Take most frequently occurring patch identifier
      tempTable <- sort(table(anchorPairDf$pid), decreasing=TRUE);
      res$pid <- names(tempTable)[1];
      ### Take most frequently occurring case identifier
      tempTable <- sort(table(anchorPairDf$cid), decreasing=TRUE);
      res$cid <- names(tempTable)[1];
      ### Get patch type
      tempTable2 <- sort(table(anchorPairDf$patchType), decreasing=TRUE);
      res$patchType <- names(tempTable2)[1];
      if (tempTable[1] < 2) {
        warning("The most frequently occurring patch identifier only occurred ",
                "once; for the anchor pair starting with anchor '",
                unique(anchorPairDf$anchors_persistent),
                "', the patch identifier I selected ('", names(tempTable)[1],
                "') may be incorrect (full list: ",
                rock::vecTxtQ(names(tempTable)), ").");
      }
      return(res);
    }
  );


dat5 <-
  rock::rbind_df_list(dat4);

coreCodes <-
  gsub("SC_(.*)_abs",
       "\\1",
       allTheCodes[
         grepl(
           "SC_[a-zA-Z]+_abs$",
           allTheCodes
         )
       ]
  );

### Loop through all six core codes
for (currentCodeCode in coreCodes) {
  
  cat("\nProcessing ", currentCodeCode, ".\n", sep="");
  
  newColName_pres <- paste0(currentCodeCode, "_align_pres");
  newColName_abs <- paste0(currentCodeCode, "_align_abs");
  
  SC_col_pres <- paste0("SC_", currentCodeCode, "_pres_SC");
  SC_col_abs <- paste0("SC_", currentCodeCode, "_abs_SC");
  
  HCI_col_pres <- paste0("HCI_", currentCodeCode, "_HCI");
  
  TA_col_pres <- paste0("TA_", currentCodeCode, "_pres");
  TA_col_abs <- paste0("TA_", currentCodeCode, "_abs");
  
  cat("  Looking at presence alignment...", sep="");
  
  if ((HCI_col_pres %in% names(dat5)) && (TA_col_pres %in% names(dat5))) {
    
    dat5[, newColName_pres] <-
      dat5[, SC_col_pres] & (dat5[, HCI_col_pres] | dat5[, TA_col_pres]);
    
    cat("\n    Coded in both HCI and TA; set to 1 when presence = 1 in SC *AND* (1 in HCI *OR* 1 in TA).", sep="");
    
  } else if (!(HCI_col_pres %in% names(dat5)) && (TA_col_pres %in% names(dat5))) {
    
    dat5[, newColName_pres] <-
      dat5[, SC_col_pres] & dat5[, TA_col_pres];

    cat("\n    Coded in TA but not HCI; set to 1 when presence = 1 in SC *AND* 1 in TA.", sep="");

  } else if ((HCI_col_pres %in% names(dat5)) && !(TA_col_pres %in% names(dat5))) {
    
    dat5[, newColName_pres] <-
      dat5[, SC_col_pres] & dat5[, HCI_col_pres];
    
    cat("\n    Coded in HCI but not TA; set to 1 when presence = 1 in SC *AND* 1 in HCI.", sep="");
    
  } else if (!(HCI_col_pres %in% names(dat5)) && !(TA_col_pres %in% names(dat5))) {
    
    dat5[, newColName_pres] <- FALSE;
      
  }

  cat(" Done.\n  Looking at absence alignment...", sep="");
  
  if (TA_col_abs %in% names(dat5)) {
    
    dat5[, newColName_abs] <- dat5[, SC_col_abs] & dat5[, TA_col_abs];
    
    cat("\n    Coded in TA; set to 1 when absence = 1 in SC *AND* absence = 1 in TA.", sep="");
    
  } else {

    dat5[, newColName_abs] <- FALSE;
    
    cat("\n    Not coded in TA; set to 0.", sep="");
    
  }
  
  dat5[, newColName_pres] <- as.numeric(dat5[, newColName_pres]);
  dat5[, newColName_abs] <- as.numeric(dat5[, newColName_abs]);
  
  cat(" Done.");
  
}

### Number of unique patch identifiers
length(unique(dat5$pid));

### Number of patch visits (each anchor pair contitutes one patch visit)
nrow(dat5);

### Patch types
table(dat5$patchType);

### Remove SERP and engine patch Types
dat6 <-
  dat5[
    !(dat5$patchType %in% c('engine', 'SERP')),
  ];

### Check
table(dat6$patchType);

presCodes <- paste0(coreCodes, "_align_pres");
absCodes <- paste0(coreCodes, "_align_abs");
SEA_codes <- c(presCodes, absCodes);

### Compute the proportion of 1s for every code per patch visit (i.e., anchor pair),
### which is the Stimulus Engagement Alignment value for that participant for that code.
dat7 <-
  by(
    dat6,
    INDICES = dat6$cid,
    FUN = function(df) {
      return(
        t(
          colMeans(
            df[, SEA_codes],
            na.rm=TRUE
          )
        )
      );
    }
  );

dat8 <-
  rock::rbind_df_list(lapply(dat7, \(x) return(data.frame(x))));

heatmap(
  as.matrix(dat8)
);

writexl::write_xlsx(
  dat8,
  path = file.path(dataPath, "heatmap_1.xlsx")
);


# Pretty heatmap
heatmap(
  as.matrix(dat8),
  Colv=NA, Rowv=NA,
);

pheatmap(
  dat8,
  cluster_cols=FALSE,
  cluster_rows=FALSE,
);

library(pheatmap)
library(svglite)
library(grid)

# Create heatmap and store it as a grid object
heatmap <- pheatmap(
  dat8,
  cluster_cols = FALSE,
  cluster_rows = FALSE
)

# Specify the filename
svg_file <- "heatmap.svg"

# Export heatmap as SVG
svglite::svglite(svg_file, width = 6, height = 6)
grid::grid.draw(heatmap$gtable)  # Use grid.draw() to render the heatmap
dev.off()  # Close the SVG device




### Compute quality value per patch; this is the sum of present codes minus the
### sum of absent codes, for all codes except advertisements, which is keyed
### inversely (i.e. presence is penalized with 1 point and absence if rewarded
### with 1 point).


writexl::write_xlsx(
  dat$attributesDf,
  path = file.path(resultsPath, "attributeDf.xlsx")
);


originalSources <-
  dat$parsedSources;

originalSources <-
  originalSources[grepl("^[[:digit:]][[:digit:]][[:digit:]]_SC", names(originalSources))];

names(originalSources) <- sourceIds;

individual_histograms <-
  lapply(
    originalSources,
    rock::code_freq_hist,
    codes = "^SC_"
  );

for (i in names(originalSources)) {
  
  ggplot2::ggsave(
    filename = file.path(resultsPath,
                         paste0("histogram-", i, ".svg")),
    plot = individual_histograms[[i]],
    width = 8,
    height = 8,
    units = "cm"
  );
  
}


### Compute quality value per patch; this is the sum of present codes minus the
### sum of absent codes, for all codes except advertisements, which is keyed
### inversely (i.e. presence is penalized with 1 point and absence if rewarded
### with 1 point).


writexl::write_xlsx(
  dat$attributesDf,
  path = file.path(resultsPath, "attributeDf.xlsx")
);


########### Creating a merged dataframe with all QUEST tables

library(readxl)
library(dplyr)

# Path to the directory containing Excel files
directory_path <- "D:/Sync/SOS-TIPS/SOS-TIPS_repo/Results/intermediate-results/QUEST/QUEST_tables"

# Get a list of all Excel files in the directory
files <- list.files(path = directory_path, pattern = "*.xlsx", full.names = TRUE)

# Function to read and process each Excel file
read_and_process_file <- function(file) {
  df <- read_excel(file)
  return(df)
}

# Read all Excel files and store them in a list of data frames
# dfs <- lapply(files, read_and_process_file)
dfs <- lapply(dfs, function(df) select(df, "fromState", "toState"))

# Identify all unique categorical values
# all_categories <- bind_rows(dfs) %>% select(1, 2) %>% distinct()
all_categories <- bind_rows(dfs) %>% select("fromState", "toState") %>% distinct()

print(colnames(all_categories))  # Print column names
print(dim(all_categories))       # Print dimensions (rows, columns)

# Create an empty master table with all unique categories
master_table <- all_categories

# Merge numerical values from each table into the master table
for (df in dfs) {
  colnames(df)[1:2] <- c("fromState", "toState")
  colnames(master_table)[1:2] <- c("fromState", "toState")
  
  master_table <- master_table %>%
    left_join(df, by = c("fromState", "toState"))
}

# Arrange rows alphabetically by the categorical columns
master_table <- master_table %>%
  arrange(fromState, toState)


# Save the master table to a new Excel file
write.csv(master_table, "D:/Sync/SOS-TIPS/SOS-TIPS_repo/Results/intermediate-results/QUEST/QUEST_tables.csv", row.names = FALSE)

---------------------------
# Calculate the sum of each "nrOfTransitions" column
nrOfTransitions_sum <- master_table %>%
  select(starts_with("nrOfTransitions")) %>%
  summarise(across(everything(), sum, na.rm = TRUE))

# Save the master table to a new CSV file
write.csv(nrOfTransitions_sum, "D:/Sync/SOS-TIPS/SOS-TIPS_repo/Results/intermediate-results/QUEST/QUEST_tables_Sums.csv", row.names = FALSE)



---------------------------------------------------------------------------------------------------------------------
  ----------------------------------------------------------------------------------------------------------------

library(dplyr)
library(tidyr)
library(readxl)

# Example directory path containing Excel files
directory_path <- "D:/Sync/SOS-TIPS/SOS-TIPS_repo/Results/intermediate-results/QUEST/QUEST_tables"

# Get a list of all Excel files in the directory
files <- list.files(path = directory_path, pattern = "*.xlsx", full.names = TRUE)

# Function to read and process each Excel file
read_and_process_file <- function(file) {
  df <- read_excel(file)
  return(df)
}

# Read all Excel files and store them in a list of data frames
dfs <- lapply(files, read_and_process_file)

# Extract all unique categories (fromState, toState) from all sources
all_categories <- bind_rows(dfs) %>%
  select(fromState, toState) %>%
  distinct()

# Create a data frame with all unique combinations of fromState and toState
master_table <- expand.grid(fromState = unique(all_categories$fromState),
                            toState = unique(all_categories$toState))

# Initialize a list to store data frames for each source
source_data <- list()

# Loop through each source to merge its data into master_table
for (i in seq_along(dfs)) {
  source <- dfs[[i]]
  
  # Left join source data into master_table
  merged_data <- merge(master_table, source, by = c("fromState", "toState"), all.x = TRUE)
  
  # Rename numerical columns with source indicator
  col_names <- colnames(merged_data)[-c(1, 2)]  # Exclude fromState and toState
  col_names <- paste0(col_names, "_source", i)
  colnames(merged_data)[-c(1, 2)] <- col_names
  
  # Replace missing values (NA) with appropriate values if needed
  # merged_data[is.na(merged_data)] <- 0  # Example: replace with 0
  
  # Store merged data in source_data list
  source_data[[i]] <- merged_data
}

# Combine all source-specific data frames into one master data frame
final_master_table <- Reduce(function(x, y) merge(x, y, by = c("fromState", "toState"), all = TRUE), source_data)

# Save the final master table to a new csv file
write.csv(final_master_table, "D:/Sync/SOS-TIPS/SOS-TIPS_repo/Results/intermediate-results/QUEST/QUEST_tables_new.csv", row.names = FALSE)



sum_nrOfTransitions <- function(df) {
  # Filter columns starting with "nrOfTransitions_"
  nrOfTransitions_cols <- grep("^nrOfTransitions_", names(df), value = TRUE)
  
  # Sum values in these columns
  sums <- colSums(df[nrOfTransitions_cols], na.rm = TRUE)
  
  return(sums)
}


sums_by_source <- sum_nrOfTransitions(final_master_table)

print(sums_by_source)

-----------------
  # Function to sum columns starting with "nrOfTransitions_" for each source
  sum_nrOfTransitions <- function(df) {
    # Filter columns starting with "nrOfTransitions_"
    nrOfTransitions_cols <- grep("^nrOfTransitions_", names(df), value = TRUE)
    
    # Sum values in these columns
    sums <- colSums(df[nrOfTransitions_cols], na.rm = TRUE)
    
    return(sums)
  }

# Sum columns for each source in final_master_table
sums_by_source <- sum_nrOfTransitions(final_master_table)

# Update final_master_table with sums
final_master_table[nrow(final_master_table) + 1, names(sums_by_source)] <- sums_by_source

# Save the updated final_master_table to CSV file
write.csv(final_master_table, "D:/Sync/SOS-TIPS/SOS-TIPS_repo/Results/intermediate-results/QUEST/QUEST_tables_new_wSums.csv", row.names = FALSE)

-----------------

TotalTransCountsExperts <- c(20, 25, 7, 32, 23, 10, 12, 15, 46, 14)
TotalTransCountsNovices <- c(10, 16, 20, 6, 19, 24, 10, 26, 13, 25)

mean(TotalTransCountsExperts) = 20.4
sd(TotalTransCountsExperts) = 11.76813
sum(TotalTransCountsExperts) = 204


mean(TotalTransCountsNovices) = 16.9
sd(TotalTransCountsNovices) = 7.015063
sum(TotalTransCountsNovices) = 169

-----------------
  
# Load data from CSV file
  df_Exp <- read.csv("D:/Sync/SOS-TIPS/SOS-TIPS_repo/Results/intermediate-results/QUEST/QUEST_tables_new_forHeatmap_Expert.csv", header = TRUE)

  df_Nov <- read.csv("D:/Sync/SOS-TIPS/SOS-TIPS_repo/Results/intermediate-results/QUEST/QUEST_tables_new_forHeatmap_Novice.csv", header = TRUE)

# Replace NA values with zeros
df[is.na(df)] <- 0

# Extract row identifiers (fromState and toState)
rows <- df[, 1]

# Extract columns starting with "propOfTransitions_"
columns <- df[, 2:21]

library(pheatmap)

pheatmap(
  df_Exp,
  cluster_cols=FALSE,
  cluster_rows=FALSE,
);

pheatmap(
  df_Nov,
  cluster_cols=FALSE,
  cluster_rows=FALSE,
);

# Create heatmap
pheatmap(as.matrix(columns),
         clustering_method = "complete",  # Method for clustering rows and columns
         clustering_distance_rows = "euclidean",  # Distance metric for row clustering
         clustering_distance_cols = "euclidean",  # Distance metric for column clustering
         scale = "row",               # Scaling method ("none", "row", "column", "both")
         show_colnames = TRUE,        # Display column names
         show_rownames = TRUE,        # Display row names
         annotation_col = NULL,       # Optional column annotations
         annotation_row = NULL,       # Optional row annotations
         main = "Heatmap Example",    # Title of the heatmap
         fontsize = 8,                # Font size for labels
         border_color = NA,           # Color of border around each cell
         NA_col = "white"             # Color for NA values (treated as zeros)
)


------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------

# Stacked bar charts of totalTime
# Experts

library(ggplot2)
library(tidyr)

Expert_totalTime <- read.csv("D:/Sync/SOS-TIPS/SOS-TIPS_repo/Results/intermediate-results/TotalTime/totalTime_barChart_df_expert.csv", header = TRUE, stringsAsFactors = FALSE)

print(colnames(Expert_totalTime))

# Reshape the dataframe from wide to long format
Expert_totalTime_long <- pivot_longer(Expert_totalTime, 
                                      cols = -Cases,  # Columns to pivot (excluding Cases)
                                      names_to = "Category",  # Name of new column for categories
                                      values_to = "TotalTime")  # Name of new column for values

# Convert Cases to factor with levels in the order they appear
Expert_totalTime_long$Cases <- factor(Expert_totalTime_long$Cases, levels = unique(Expert_totalTime$Cases))

# Create the stacked bar chart using ggplot2
Expert_totalTime_plot <- ggplot(Expert_totalTime_long, aes(x = Cases, y = TotalTime, fill = Category)) +
  geom_bar(stat = "identity") + 
  scale_fill_brewer(palette = "BrBG") +  
  labs(x = "Case",
       y = "Time (seconds)",
       fill = "Patch content type") +
  theme_minimal() +  
  theme(
    axis.title.x = element_text(size = 12),  
    axis.title.y = element_text(size = 12),
    axis.text.x = element_text(size = 10, angle = 45, hjust = 1),
    axis.text.y = element_text(size = 10),
    legend.position = "right"
  )

# Print the plot to verify
print(Expert_totalTime_plot)


# Stacked bar charts of totalTime
# Novices

library(ggplot2)
library(tidyr)

Novice_totalTime <- read.csv("D:/Sync/SOS-TIPS/SOS-TIPS_repo/Results/intermediate-results/TotalTime/totalTime_barChart_df_novice.csv", header = TRUE, stringsAsFactors = FALSE)

print(colnames(Novice_totalTime))

# Reshape the dataframe from wide to long format
Novice_totalTime_long <- pivot_longer(Novice_totalTime, 
                                      cols = -Cases,  # Columns to pivot (excluding Cases)
                                      names_to = "Category",  # Name of new column for categories
                                      values_to = "TotalTime")  # Name of new column for values

# Convert Cases to factor with levels in the order they appear
Novice_totalTime_long$Cases <- factor(Novice_totalTime_long$Cases, levels = unique(Novice_totalTime_long$Cases))

# Create the stacked bar chart using ggplot2
Novice_totalTime_long_plot <- ggplot(Novice_totalTime_long, aes(x = Cases, y = TotalTime, fill = Category)) +
  geom_bar(stat = "identity") + 
  scale_fill_brewer(palette = "BrBG") +  
  labs(x = "Case",
       y = "Time (seconds)",
       fill = "Patch content type") +
  theme_minimal() +  
  theme(
    axis.title.x = element_text(size = 12),  
    axis.title.y = element_text(size = 12),
    axis.text.x = element_text(size = 10, angle = 45, hjust = 1),
    axis.text.y = element_text(size = 10),
    legend.position = "right"
  )

# Print the plot to verify
print(Novice_totalTime_long_plot)
