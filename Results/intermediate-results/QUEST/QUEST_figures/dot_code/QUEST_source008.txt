digraph {
  node[fontname=Arial]

  edge[fontname=Arial]

  academia -> engine_SERP [label="  0.50    ", penwidth=1.5];
  academia -> scicomm [label="  0.50    ", penwidth=1.5];
  engine_SERP -> academia [label="  0.12    ", penwidth=0.375];
  engine_SERP -> engine_SERP [label="  0.50    ", penwidth=1.5];
  engine_SERP -> government [label="  0.12    ", penwidth=0.375];
  engine_SERP -> news [label="  0.25    ", penwidth=0.75];
  government -> engine_SERP [label="  0.50    ", penwidth=1.5];
  government -> government [label="  0.50    ", penwidth=1.5];
  news -> academia [label="  0.50    ", penwidth=1.5];
  news -> engine_SERP [label="  0.50    ", penwidth=1.5];
  scicomm -> engine_SERP [label="  1.00    ", penwidth=3];
}

