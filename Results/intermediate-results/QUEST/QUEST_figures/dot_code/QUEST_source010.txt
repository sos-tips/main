digraph {
  node[fontname=Arial]

  edge[fontname=Arial]

  engine_SERP -> engine_SERP [label="  0.67    ", penwidth=2.0000001];
  engine_SERP -> scicomm [label="  0.33    ", penwidth=0.9999999];
  government -> news [label="  1.00    ", penwidth=3];
  news -> scicomm [label="  1.00    ", penwidth=3];
  scicomm -> government [label="  0.11    ", penwidth=0.3333333];
  scicomm -> scicomm [label="  0.89    ", penwidth=2.6666667];
}

