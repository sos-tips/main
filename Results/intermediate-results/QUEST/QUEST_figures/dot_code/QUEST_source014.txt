digraph {
  node[fontname=Arial]

  edge[fontname=Arial]

  engine_SERP -> engine_SERP [label="  0.33    ", penwidth=0.9999999];
  engine_SERP -> government [label="  0.33    ", penwidth=0.9999999];
  engine_SERP -> scicomm [label="  0.33    ", penwidth=0.9999999];
  government -> engine_SERP [label="  0.50    ", penwidth=1.5];
  government -> government [label="  0.50    ", penwidth=1.5];
  other -> engine_SERP [label="  1.00    ", penwidth=3];
}

