digraph {
  node[fontname=Arial]

  edge[fontname=Arial]

  academia -> academia [label="  0.62    ", penwidth=1.8461538];
  academia -> engine_SERP [label="  0.15    ", penwidth=0.4615386];
  academia -> scicomm [label="  0.23    ", penwidth=0.6923076];
  engine_SERP -> academia [label="  0.29    ", penwidth=0.8571429];
  engine_SERP -> engine_SERP [label="  0.43    ", penwidth=1.2857142];
  engine_SERP -> government [label="  0.14    ", penwidth=0.4285713];
  engine_SERP -> scicomm [label="  0.14    ", penwidth=0.4285713];
  government -> academia [label="  1.00    ", penwidth=3];
  scicomm -> academia [label="  0.50    ", penwidth=1.5];
  scicomm -> engine_SERP [label="  0.50    ", penwidth=1.5];
}

