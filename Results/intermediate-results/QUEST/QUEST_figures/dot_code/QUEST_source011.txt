digraph {
  node[fontname=Arial]

  edge[fontname=Arial]

  academia -> engine_SERP [label="  0.50    ", penwidth=1.5];
  academia -> news [label="  0.50    ", penwidth=1.5];
  engine_SERP -> government [label="  0.33    ", penwidth=0.9999999];
  engine_SERP -> news [label="  0.33    ", penwidth=0.9999999];
  engine_SERP -> other [label="  0.33    ", penwidth=0.9999999];
  government -> engine_SERP [label="  1.00    ", penwidth=3];
  news -> academia [label="  0.67    ", penwidth=2.0000001];
  news -> news [label="  0.33    ", penwidth=0.9999999];
  other -> engine_SERP [label="  1.00    ", penwidth=3];
}

