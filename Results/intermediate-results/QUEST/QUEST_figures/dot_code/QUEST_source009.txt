digraph {
  node[fontname=Arial]

  edge[fontname=Arial]

  academia -> academia [label="  0.38    ", penwidth=1.15384614];
  academia -> engine_SERP [label="  0.38    ", penwidth=1.15384614];
  academia -> news [label="  0.08    ", penwidth=0.23076924];
  academia -> scicomm [label="  0.15    ", penwidth=0.46153845];
  engine_SERP -> academia [label="  0.25    ", penwidth=0.75];
  engine_SERP -> engine_SERP [label="  0.71    ", penwidth=2.14285713];
  engine_SERP -> news [label="  0.04    ", penwidth=0.10714287];
  news -> engine_SERP [label="  0.67    ", penwidth=2.00000001];
  news -> news [label="  0.33    ", penwidth=0.99999999];
  scicomm -> academia [label="  0.50    ", penwidth=1.5];
  scicomm -> engine_SERP [label="  0.50    ", penwidth=1.5];
}

