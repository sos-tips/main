[[sourceId=source007]]
[[streamId=SC]]
[[cid=case007]]

---
ROCK_attributes:
  -
    pid: "patch_007_1"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
    totalTime: "00:12"
    contentType: "engine/SERP"
  -
    pid: "patch_007_2"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
    totalTime: "00:04"
    contentType: "engine/SERP"
  -
    pid: "patch_007_3"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
    totalTime: "00:06"
    contentType: "engine/SERP"
  -
    pid: "patch_007_4"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:59"
    contentType: "engine/SERP"
  -
    pid: "patch_007_5"
    patchTag: "School of Veterinary Medicine"
    domain: "vetmed.wisc.edu"
    patchType: "page"
    totalTime: "00:33"
    contentType: "scicomm"
  -
    pid: "patch_007_6"
    patchTag: "Centers for Disease Control and Prevention"
    domain: "cdc.gov"
    patchType: "page"
    totalTime: "00:42"
    contentType: "government"
  -
    pid: "patch_007_7"
    patchTag: "Centers for Disease Control and Prevention"
    domain: "cdc.gov"
    patchType: "page"
    totalTime: "00:54"
    contentType: "government"
  -
    pid: "patch_007_8"
    patchTag: "National Library of Medicine"
    domain: "ncbi.nlm.nih.gov"
    patchType: "page"
    totalTime: "00:42"
    contentType: "academia"
  -
    pid: "patch_007_9"
    patchTag: "Contagion Live"
    domain: "contagionlive.com"
    patchType: "page"
    totalTime: "00:33"
    contentType: "scicomm"
  -
    pid: "patch_007_10"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:42"
    contentType: "engine/SERP"
  -
    pid: "patch_007_11"
    patchTag: "National Library of Medicine"
    domain: "ncbi.nlm.nih.gov"
    patchType: "page"
    totalTime: "00:16"
    contentType: "academia"
  -
    pid: "patch_007_12"
    patchTag: "WIRED"
    domain: "wired.com"
    patchType: "page"
    totalTime: "01:53"
    contentType: "news"
  -
    pid: "patch_007_13"
    patchTag: "Director of National Intelligence"
    domain: "dni.gov"
    patchType: "file"
    totalTime: "01:03"
    contentType: "government"
  -
    pid: "patch_007_14"
    patchTag: "The Intercept"
    domain: "theintercept.com"
    patchType: "page"
    totalTime: "01:09"
    contentType: "news"
---

[[pid: patch_007_1]]
--+-{ anchor_007_1 }-+--
[[state:engine_SERP]]

[[pid: patch_007_2]]
--+-{ anchor_007_2 }-+--
[[state:engine_SERP]]

[[pid: patch_007_3]]
--+-{ anchor_007_3 }-+--
[[state:engine_SERP]]

[[pid: patch_007_4]]
--+-{ anchor_007_4 }-+--
[[state:engine_SERP]]

[[pid: patch_007_5]]
--+-{ anchor_007_5 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_abs]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_abs]] [[APPR>SC_Refs_pres]] [[state:scicomm]]

[[pid: patch_007_6]]
--+-{ anchor_007_6 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_abs]] [[APPR>SC_Author_abs]] [[APPR>SC_AuthAffil_abs]] [[state:government]]

[[pid: patch_007_7]]
--+-{ anchor_007_7 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_abs]] [[APPR>SC_Author_abs]] [[APPR>SC_AuthAffil_abs]] [[APPR>SC_Refs_pres]] [[state:government]]

[[pid: patch_007_8]]
--+-{ anchor_007_9 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_AuthAffil_pres]] [[APPR>SC_DOI_pres]] [[APPR>SC_Refs_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_abs]] [[APPR>SC_SciMet_pres]] [[state:academia]]

[[pid: patch_007_4]]
--+-{ anchor_007_10 }-+--
[[state:engine_SERP]]

[[pid: patch_007_9]]
--+-{ anchor_007_11 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Refs_pres]] [[state:scicomm]]

[[pid: patch_007_10]]
--+-{ anchor_007_12 }-+--
[[state:engine_SERP]]

[[pid: patch_007_11]]
--+-{ anchor_007_13 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Refs_pres]] [[APPR>SC_Ads_abs]] [[APPR>SC_AuthAffil_abs]] [[state:academia]]

[[pid: patch_007_12]]
--+-{ anchor_007_14 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Refs_pres]] [[APPR>SC_Ads_pres]] [[state:news]]

[[pid: patch_007_13]]
--+-{ anchor_007_15 }-+--
[[APPR>SC_AuthAffil_abs]] [[APPR>SC_Author_abs]] [[APPR>SC_URL_pres]] [[APPR>SC_Date_abs]] [[APPR>SC_DOI_abs]] [[APPR>SC_Refs_abs]] [[state:government]]

[[pid: patch_007_14]]
--+-{ anchor_007_16 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_AuthAffil_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Refs_pres]] [[APPR>SC_Ads_abs]] [[state:news]]
