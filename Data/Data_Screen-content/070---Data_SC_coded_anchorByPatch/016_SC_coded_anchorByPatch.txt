[[sourceId=source016]]
[[streamId=SC]]
[[cid=case016]]


---
ROCK_attributes:
  -
    pid: "patch_016_1"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
    totalTime: "00:13"
    contentType: "engine/SERP"
  -
    pid: "patch_016_2"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:14"
    contentType: "engine/SERP"
  -
    pid: "patch_016_3"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:12"
    contentType: "engine/SERP"
  -
    pid: "patch_016_4"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:41"
    contentType: "engine/SERP"
  -
    pid: "patch_016_5"
    patchTag: "BBC"
    domain: "bbc.com"
    patchType: "page"
    totalTime: "02:02"
    contentType: "news"
  -
    pid: "patch_016_6"
    patchTag: "Science"
    domain: "science.org"
    patchType: "page"
    totalTime: "00:46"
    contentType: "academia"
  -
    pid: "patch_016_7"
    patchTag: "BBC"
    domain: "bbc.com"
    patchType: "page"
    totalTime: "00:31"
    contentType: "news"
  -
    pid: "patch_016_8"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:24"
    contentType: "engine/SERP"
  -
    pid: "patch_016_9"
    patchTag: "The Guardian"
    domain: "theguardian.com"
    patchType: "page"
    totalTime: "01:06"
    contentType: "news"
  -
    pid: "patch_016_10"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:24"
    contentType: "engine/SERP"
  -
    pid: "patch_016_11"
    patchTag: "GOV.UK"
    domain: "gov.uk"
    patchType: "page"
    totalTime: "00:08"
    contentType: "government"
  -
    pid: "patch_016_12"
    patchTag: "GOV.UK"
    domain: "gov.uk"
    patchType: "page"
    totalTime: "00:26"
    contentType: "government"
  -
    pid: "patch_016_13"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:16"
    contentType: "engine/SERP"
  -
    pid: "patch_016_14"
    patchTag: "National Geographic"
    domain: "nationalgeographic.com"
    patchType: "page"
    totalTime: "00:23"
    contentType: "scicomm"
  -
    pid: "patch_016_15"
    patchTag: "Director of National Intelligence"
    domain: "dni.gov"
    patchType: "file"
    totalTime: "00:42"
    contentType: "government"
  -
    pid: "patch_016_16"
    patchTag: "The Guardian"
    domain: "theguardian.com"
    patchType: "page"
    totalTime: "00:37"
    contentType: "news"
---

[[pid: patch_016_1]]
--+-{ anchor_016_1 }-+--
[[state:engine_SERP]]

[[pid: patch_016_2]]
--+-{ anchor_016_2 }-+--
[[state:engine_SERP]]

[[pid: patch_016_3]]
--+-{ anchor_016_3 }-+--
[[state:engine_SERP]]

[[pid: patch_016_4]]
--+-{ anchor_016_4 }-+--
[[state:engine_SERP]]

[[pid: patch_016_5]]
--+-{ anchor_016_5 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_AuthAffil_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_pres]] [[APPR>SC_Refs_pres]] [[state:news]]

[[pid: patch_016_4]]
--+-{ anchor_016_6 }-+--
[[state:engine_SERP]]

[[pid: patch_016_5]]
--+-{ anchor_016_7 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_AuthAffil_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_pres]] [[APPR>SC_Refs_pres]] [[state:news]]

[[pid: patch_016_6]]
--+-{ anchor_016_8 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_AuthAffil_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_DOI_pres]] [[APPR>SC_SciMet_pres]] [[APPR>SC_Ads_abs]] [[APPR>SC_Refs_pres]] [[state:academia]]

[[pid: patch_016_5]]
--+-{ anchor_016_9 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_AuthAffil_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_pres]] [[APPR>SC_Refs_pres]] [[state:news]]

[[pid: patch_016_7]]
--+-{ anchor_016_10 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_AuthAffil_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_pres]] [[APPR>SC_Refs_pres]] [[state:news]]

[[pid: patch_016_4]]
--+-{ anchor_016_11 }-+--
[[state:engine_SERP]]

[[pid: patch_016_8]]
--+-{ anchor_016_12 }-+--
[[state:engine_SERP]]

[[pid: patch_016_9]]
--+-{ anchor_016_13 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_pres]] [[APPR>SC_Refs_pres]] [[APPR>SC_Author_abs]] [[APPR>SC_AuthAffil_abs]] [[state:news]]

[[pid: patch_016_8]]
--+-{ anchor_016_14 }-+--
[[state:engine_SERP]]

[[pid: patch_016_10]]
--+-{ anchor_016_15 }-+--
[[state:engine_SERP]]

[[pid: patch_016_11]]
--+-{ anchor_016_16 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_abs]] [[APPR>SC_AuthAffil_abs]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_abs]] [[APPR>SC_Refs_pres]] [[state:government]]

[[pid: patch_016_12]]
--+-{ anchor_016_17 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_abs]] [[APPR>SC_AuthAffil_abs]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_abs]] [[APPR>SC_Refs_pres]] [[state:government]]

[[pid: patch_016_11]]
--+-{ anchor_016_18 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_abs]] [[APPR>SC_AuthAffil_abs]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_abs]] [[APPR>SC_Refs_pres]] [[state:government]]

[[pid: patch_016_10]]
--+-{ anchor_016_19 }-+--
[[state:engine_SERP]]

[[pid: patch_016_13]]
--+-{ anchor_016_20 }-+--
[[state:engine_SERP]]

[[pid: patch_016_14]]
--+-{ anchor_016_21 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_AuthAffil_abs]] [[APPR>SC_Refs_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_pres]] [[state:scicomm]]

[[pid: patch_016_15]]
--+-{ anchor_016_22 }-+--
[[APPR>SC_Date_abs]] [[APPR>SC_Author_abs]] [[APPR>SC_AuthAffil_abs]] [[APPR>SC_DOI_abs]] [[APPR>SC_URL_pres]] [[APPR>SC_Refs_abs]] [[state:government]]

[[pid: patch_016_14]]
--+-{ anchor_016_23 }-+--
[[APPR>SC_Date_pres]] [[APPR>SC_Author_pres]] [[APPR>SC_AuthAffil_abs]] [[APPR>SC_Refs_pres]] [[APPR>SC_URL_pres]] [[APPR>SC_Ads_pres]] [[state:scicomm]]

[[pid: patch_016_13]]
--+-{ anchor_016_24 }-+--
[[state:engine_SERP]]

[[pid: patch_016_16]]
--+-{ anchor_016_25 }-+--
[[state:news]]

