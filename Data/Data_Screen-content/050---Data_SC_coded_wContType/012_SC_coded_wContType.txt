[[sourceId=source012]]
[[streamId=SC]]
[[cid=case012]]


---
ROCK_attributes:
  -
    pid: "patch_012_1"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:13"
    contentType: "engine/SERP"
  -
    pid: "patch_012_2"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:41"
    contentType: "engine/SERP"
  -
    pid: "patch_012_3"
    patchTag: "National Institute of Allergy and Infectious Diseases"
    domain: "niad.nih.gov"
    patchType: "page"
    totalTime: "01:01"
    contentType: "government"
  -
    pid: "patch_012_4"
    patchTag: "National Institute of Allergy and Infectious Diseases"
    domain: "niad.nih.gov"
    patchType: "page"
    totalTime: "01:02"
    contentType: "government"
  -
    pid: "patch_012_5"
    patchTag: "BBC"
    domain: "bbc.com"
    patchType: "page"
    totalTime: "00:28"
    contentType: "news"
  -
    pid: "patch_012_6"
    patchTag: "Science"
    domain: "science.org"
    patchType: "page"
    totalTime: "04:06"
    contentType: "academia"
  -
    pid: "patch_012_7"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:28"
    contentType: "engine/SERP"
  -
    pid: "patch_012_8"
    patchTag: "World Health Organization"
    domain: "who.int"
    patchType: "page"
    totalTime: "00:08"
    contentType: "government"
  -
    pid: "patch_012_9"
    patchTag: "World Health Organization"
    domain: "who.int"
    patchType: "page"
    totalTime: "00:15"
    contentType: "government"
  -
    pid: "patch_012_10"
    patchTag: "World Health Organization"
    domain: "who.int"
    patchType: "page"
    totalTime: "00:46"
    contentType: "government"
  -
    pid: "patch_012_11"
    patchTag: "Wikipedia"
    domain: "en.wikipedia.org"
    patchType: "page"
    totalTime: "00:16"
    contentType: "scicomm"
---


[[pid: patch_012_1]]
--+-{ anchor_012_1 }-+--

[[pid: patch_012_2]]
--+-{ anchor_012_2 }-+--

[[pid: patch_012_3]]
--+-{ anchor_012_3 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] 

[[pid: patch_012_4]]
--+-{ anchor_012_4 }-+--
[[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>Date_pres]]

[[pid: patch_012_2]]
--+-{ anchor_012_5 }-+--

[[pid: patch_012_5]]
--+-{ anchor_012_6 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Refs_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_012_6]]
--+-{ anchor_012_7 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>DOI_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Refs_pres]] [[SC>APPR>SciMet_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_012_2]]
--+-{ anchor_012_8 }-+--

[[pid: patch_012_7]]
--+-{ anchor_012_9 }-+--

[[pid: patch_012_8]]
--+-{ anchor_012_10 }-+--
[[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_012_9]]
--+-{ anchor_012_11 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>DOI_abs]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_012_10]]
--+-{ anchor_012_12 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Refs_pres]]

[[pid: patch_012_5]]
--+-{ anchor_012_13 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Refs_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_012_6]]
--+-{ anchor_012_14 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>DOI_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Refs_pres]] [[SC>APPR>SciMet_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_012_10]]
--+-{ anchor_012_15 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Refs_pres]]

[[pid: patch_012_7]]
--+-{ anchor_012_16 }-+--

[[pid: patch_012_11]]
--+-{ anchor_012_17 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]]
