[[sourceId=source002]]
[[streamId=SC]]
[[cid=case002]]

---
ROCK_attributes:
  -
    pid: "patch_002_1"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
    totalTime: "00:20"
    contentType: "engine/SERP"
  -
    pid: "patch_002_2"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:33"
    contentType: "engine/SERP"
  -
    pid: "patch_002_3"
    patchTag: "United States Agency for International Development"
    domain: "usaid.gov"
    patchType: "file"
    totalTime: "00:27"
    contentType: "government"
  -
    pid: "patch_002_4"
    patchTag: "U.S. Department of Defense"
    domain: "media.defense.gov"
    patchType: "file"
    totalTime: "00:11"
    contentType: "government"
  -
    pid: "patch_002_5"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:26"
    contentType: "engine/SERP"
  -
    pid: "patch_002_6"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:16"
    contentType: "engine/SERP"
  -
    pid: "patch_002_7"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "02:28"
    contentType: "engine/SERP"
  -
    pid: "patch_002_8"
    patchTag: "U. S. Department of Health & Human Services"
    domain: "hhs.gov"
    patchType: "page"
    totalTime: "00:40"
    contentType: "government"
  -
    pid: "patch_002_9"
    patchTag: "The White House"
    domain: "whitehouse.gov"
    patchType: "page"
    totalTime: "00:04"
    contentType: "government"
  -
    pid: "patch_002_10"
    patchTag: "Wall Street Journal"
    domain: "wsj.com"
    patchType: "page"
    totalTime: "00:07"
    contentType: "news"
  -
    pid: "patch_002_11"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:46"
    contentType: "engine/SERP"
  -
    pid: "patch_002_12"
    patchTag: "WebMD"
    domain: "webmd.com"
    patchType: "page"
    totalTime: "00:57"
    contentType: "scicomm"
  -
    pid: "patch_002_14"
    patchTag: "National Library of Medicine"
    domain: "ncbi.nlm.nih.gov"
    patchType: "page"
    totalTime: "01:06"
    contentType: "academia"
  -
    pid: "patch_002_15"
    patchTag: "U. S. Department of Health & Human Services"
    domain: "hhs.gov"
    patchType: "SERP"
    totalTime: "00:10"
    contentType: "engine/SERP"
  -
    pid: "patch_002_16"
    patchTag: "Centers for Disease Control and Prevention"
    domain: "cdc.gov"
    patchType: "page"
    totalTime: "00:12"
    contentType: "government"
  -
    pid: "patch_002_17"
    patchTag: "Centers for Disease Control and Prevention"
    domain: "cdc.gov"
    patchType: "page"
    totalTime: "00:20"
    contentType: "government"
  -
    pid: "patch_002_18"
    patchTag: "Centers for Disease Control and Prevention"
    domain: "cdc.gov"
    patchType: "page"
    totalTime: "00:11"
    contentType: "government"
  -
    pid: "patch_002_19"
    patchTag: "Centers for Disease Control and Prevention"
    domain: "cdc.gov"
    patchType: "page"
    totalTime: "00:10"
    contentType: "government"
  -
    pid: "patch_002_20"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:23"
    contentType: "engine/SERP"
  -
    pid: "patch_002_21"
    patchTag: "Science"
    domain: "science.org"
    patchType: "page"
    totalTime: "01:45"
    contentType: "scicomm"
---

[[pid: patch_002_1]]
--+-{ anchor_002_1 }-+--
[[state:engine_SERP]]

[[pid: patch_002_2]]
--+-{ anchor_002_2 }-+--
[[state:engine_SERP]]

[[pid: patch_002_3]]
--+-{ anchor_002_3 }-+--
[[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[state:government]]

[[pid: patch_002_4]]
--+-{ anchor_002_4 }-+--
[[SC>APPR>URL_pres]] [[SC>APPR>AuthAffil_pres]] [[state:government]]

[[pid: patch_002_2]]
--+-{ anchor_002_5 }-+--
[[state:engine_SERP]]

[[pid: patch_002_5]]
--+-{ anchor_002_6 }-+--
[[state:engine_SERP]]

[[pid: patch_002_6]]
--+-{ anchor_002_7 }-+--
[[state:engine_SERP]]

[[pid: patch_002_7]]
--+-{ anchor_002_8 }-+--
[[state:engine_SERP]]

[[pid: patch_002_8]]
--+-{ anchor_002_9 }-+--
[[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Date_pres]] [[SC>APPR>Ads_abs]] [[state:government]]

[[pid: patch_002_9]]
--+-{ anchor_002_10 }-+--
[[SC>APPR>AuthAffil_pres]] [[SC>APPR>Date_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Author_abs]] [[state:government]]

[[pid: patch_002_10]]
--+-{ anchor_002_11 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Refs_pres]] [[SC>APPR>Author_abs]] [[SC>APPR>Ads_abs]] [[SC>APPR>AuthAffil_pres]] [[state:news]]

[[pid: patch_002_7]]
--+-{ anchor_002_12 }-+--
[[state:engine_SERP]]

[[pid: patch_002_11]]
--+-{ anchor_002_13 }-+--
[[state:engine_SERP]]

[[pid: patch_002_12]]
--+-{ anchor_002_14 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>AuthAffil_abs]] [[state:scicomm]]

[[pid: patch_002_14]]
--+-{ anchor_002_15 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>DOI_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>SciMet_abs]] [[state:academia]]

[[pid: patch_002_8]]
--+-{ anchor_002_16 }-+--
[[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Date_pres]] [[SC>APPR>Ads_abs]] [[state:government]]

[[pid: patch_002_15]]
--+-{ anchor_002_17 }-+--
[[SC>APPR>URL_pres]] [[state:engine_SERP]]

[[pid: patch_002_16]]
--+-{ anchor_002_18 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[state:government]]

[[pid: patch_002_17]]
--+-{ anchor_002_19 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[state:government]]

[[pid: patch_002_18]]
--+-{ anchor_002_20 }-+--
[[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Refs_pres]] [[state:government]]

[[pid: patch_002_19]]
--+-{ anchor_002_21 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[state:government]]

[[pid: patch_002_4]]
--+-{ anchor_002_22 }-+--
[[SC>APPR>URL_pres]] [[SC>APPR>AuthAffil_pres]] [[state:government]]

[[pid: patch_002_3]]
--+-{ anchor_002_23 }-+--
[[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[state:government]]

[[pid: patch_002_19]]
--+-{ anchor_002_24 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[state:government]]

[[pid: patch_002_11]]
--+-{ anchor_002_25 }-+--
[[state:engine_SERP]]

[[pid: patch_002_20]]
--+-{ anchor_002_26 }-+--
[[state:engine_SERP]]

[[pid: patch_002_21]]
--+-{ anchor_002_27 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>Ads_pres]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[state:scicomm]]
