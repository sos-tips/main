[[sourceId=source011]]
[[streamId=SC]]
[[cid=case011]]

---
ROCK_attributes:
  -
    pid: "patch_011_1"
    patchTag: "Zoom"
    domain: "zoom.us"
    patchType: "page"
    totalTime: "00:25"
  -
    pid: "patch_011_2"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "01:02"
  -
    pid: "patch_011_3"
    patchTag: "United Nations"
    domain: "un.org"
    patchType: "page"
    totalTime: "00:32"
  -
    pid: "patch_011_4"
    patchTag: "CNN"
    domain: "cnn.com"
    patchType: "page"
    totalTime: "01:05"
  -
    pid: "patch_011_5"
    patchTag: "CNN"
    domain: "cnn.com"
    patchType: "page"
    totalTime: "00:23"
  -
    pid: "patch_011_6"
    patchTag: "Zenodo"
    domain: "zenodo.org"
    patchType: "page"
    totalTime: "02:07"
  -
    pid: "patch_011_7"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
    totalTime: "00:06"
  -
    pid: "patch_011_8"
    patchTag: "Council on Foreign Relations"
    domain: "cfr.org"
    patchType: "page"
    totalTime: "03:00"
---

[[pid: patch_011_1]]
--+-{ anchor_011_1 }-+--

[[pid: patch_011_2]]
--+-{ anchor_011_2 }-+--

[[pid: patch_011_3]]
--+-{ anchor_011_3 }-+--

[[pid: patch_011_2]]
--+-{ anchor_011_4 }-+--

[[pid: patch_011_4]]
--+-{ anchor_011_5 }-+--

[[pid: patch_011_5]]
--+-{ anchor_011_6 }-+--

[[pid: patch_011_6]]
--+-{ anchor_011_7 }-+--

[[pid: patch_011_5]]
--+-{ anchor_011_8 }-+--

[[pid: patch_011_6]]
--+-{ anchor_011_9 }-+--

[[pid: patch_011_7]]
--+-{ anchor_011_10 }-+--

[[pid: patch_011_2]]
--+-{ anchor_011_11 }-+--

[[pid: patch_011_8]]
--+-{ anchor_011_12 }-+--

