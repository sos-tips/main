[[sourceId=source013]]
[[streamId=SC]]
[[cid=case013]]

---
ROCK_attributes:
  -
    pid: "patch_013_1"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
    totalTime: "00:39"
  -
    pid: "patch_013_2"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:28"
  -
    pid: "patch_013_3"
    patchTag: "National Institute of Allergy and Infectious Diseases"
    domain: "niad.nih.gov"
    patchType: "page"
    totalTime: "01:02"
  -
    pid: "patch_013_4"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:09"
  -
    pid: "patch_013_5"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:09"
  -
    pid: "patch_013_6"
    patchTag: "Wuhan Institute of Virology"
    domain: "english.whiov.cas.cn"
    patchType: "page"
    totalTime: "00:02"
  -
    pid: "patch_013_7"
    patchTag: "U.S. Department of State"
    domain: "2017-2021.state.gov"
    patchType: "page"
    totalTime: "00:52"
  -
    pid: "patch_013_8"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:11"
  -
    pid: "patch_013_9"
    patchTag: "National Library of Medicine"
    domain: "ncbi.nim.nih.gov"
    patchType: "page"
    totalTime: "00:31"
  -
    pid: "patch_013_10"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:08"
  -
    pid: "patch_013_11"
    patchTag: "Bat Conservation Trust"
    domain: "bats.org.uk"
    patchType: "page"
    totalTime: "02:05"
  -
    pid: "patch_013_12"
    patchTag: "National Institute of Allergy and Infectious Diseases"
    domain: "niad.nih.gov"
    patchType: "page"
    totalTime: "03:02"
---

[[pid: patch_013_1]]
--+-{ anchor_013_1 }-+--

[[pid: patch_013_2]]
--+-{ anchor_013_2 }-+--

[[pid: patch_013_3]]
--+-{ anchor_013_3 }-+--

[[pid: patch_013_1]]
--+-{ anchor_013_4 }-+--

[[pid: patch_013_3]]
--+-{ anchor_013_5 }-+--

[[pid: patch_013_1]]
--+-{ anchor_013_6 }-+--

[[pid: patch_013_4]]
--+-{ anchor_013_7 }-+--

[[pid: patch_013_3]]
--+-{ anchor_013_8 }-+--

[[pid: patch_013_1]]
--+-{ anchor_013_9 }-+--

[[pid: patch_013_5]]
--+-{ anchor_013_10 }-+--

[[pid: patch_013_6]]
--+-{ anchor_013_11 }-+--

[[pid: patch_013_5]]
--+-{ anchor_013_12 }-+--

[[pid: patch_013_7]]
--+-{ anchor_013_13 }-+--

[[pid: patch_013_1]]
--+-{ anchor_013_14 }-+--

[[pid: patch_013_8]]
--+-{ anchor_013_15 }-+--

[[pid: patch_013_9]]
--+-{ anchor_013_16 }-+--

[[pid: patch_013_3]]
--+-{ anchor_013_17 }-+--

[[pid: patch_013_1]]
--+-{ anchor_013_18 }-+--

[[pid: patch_013_10]]
--+-{ anchor_013_19 }-+--

[[pid: patch_013_11]]
--+-{ anchor_013_20 }-+--

[[pid: patch_013_3]]
--+-{ anchor_013_21 }-+--

[[pid: patch_013_12]]
--+-{ anchor_013_22 }-+--

