[[sourceId=source003]]
[[streamId=SC]]
[[cid=case003]]

---
ROCK_attributes:
  -
    pid: "patch_003_1"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
    totalTime: "00:16"
  -
    pid: "patch_003_2"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:04"
  -
    pid: "patch_003_3"
    patchTag: "Medical News Today"
    domain: "medicalnewstoday.com"
    patchType: "page"
    totalTime: "02:20"
  -
    pid: "patch_003_4"
    patchTag: "World Health Organization"
    domain: "who.int"
    patchType: "page"
    totalTime: "00:17"
  -
    pid: "patch_003_5"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:08"
  -
    pid: "patch_003_6"
    patchTag: "Nature"
    domain: "nature.com"
    patchType: "page"
    totalTime: "03:53"
  -
    pid: "patch_003_7"
    patchTag: "Aljazeera"
    domain: "aljazeera.com"
    patchType: "page"
    totalTime: "02:51"
---

[[pid: patch_003_1]]
--+-{ anchor_003_1 }-+--

[[pid: patch_003_2]]
--+-{ anchor_003_2 }-+--

[[pid: patch_003_3]]
--+-{ anchor_003_3 }-+--

#The next website was loading for a long time, he checked it twice, but as it was still loading, he clicked back to read this page while the next one was loading (try to move to next patch between 1:22-1:37 and 1:55-1:56).

[[pid: patch_003_4]]
--+-{ anchor_003_4 }-+--

[[pid: patch_003_5]]
--+-{ anchor_003_5 }-+--

[[pid: patch_003_6]]
--+-{ anchor_003_6 }-+--

[[pid: patch_003_5]]
--+-{ anchor_003_7 }-+--

[[pid: patch_003_7]]
--+-{ anchor_003_8 }-+--

