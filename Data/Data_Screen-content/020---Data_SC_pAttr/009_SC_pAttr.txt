[[sourceId=source009]]
[[streamId=SC]]
[[cid=case009]]

---
ROCK_attributes:
  -
    pid: "patch_009_1"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
  -
    pid: "patch_009_2"
    patchTag: "UW-Madison Libraries"
    domain: "library.wisc.edu"
    patchType: "engine"
  -
    pid: "patch_009_3"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "SERP"
  -
    pid: "patch_009_4"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "SERP"
  -
    pid: "patch_009_5"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "page"
  -
    pid: "patch_009_6"
    patchTag: "ScienceDirect"
    domain: "sciencedirect.com"
    patchType: "engine"
  -
    pid: "patch_009_7"
    patchTag: "ScienceDirect"
    domain: "sciencedirect.com"
    patchType: "SERP"
  -
    pid: "patch_009_8"
    patchTag: "ScienceDirect"
    domain: "sciencedirect.com"
    patchType: "page"
  -
    pid: "patch_009_9"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "page"
  -
    pid: "patch_009_10"
    patchTag: "Web of Science"
    domain: "webofscience.com"
    patchType: "engine"
  -
    pid: "patch_009_11"
    patchTag: "Web of Science"
    domain: "webofscience.com"
    patchType: "SERP"
  -
    pid: "patch_009_12"
    patchTag: "Web of Science"
    domain: "webofscience.com"
    patchType: "page"
  -
    pid: "patch_009_13"
    patchTag: "Web of Science"
    domain: "webofscience.com"
    patchType: "page"
  -
    pid: "patch_009_14"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
  -
    pid: "patch_009_15"
    patchTag: "Google News"
    domain: "news.google.com"
    patchType: "engine"
  -
    pid: "patch_009_16"
    patchTag: "Google News"
    domain: "news.google.com"
    patchType: "SERP"
  -
    pid: "patch_009_17"
    patchTag: "The New York Times"
    domain: "newyorktimes.com"
    patchType: "page"
  -
    pid: "patch_009_18"
    patchTag: "PBS News Hour"
    domain: "pbs.org"
    patchType: "page"
  -
    pid: "patch_009_19"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
  -
    pid: "patch_009_20"
    patchTag: "Google Scholar"
    domain: "scholar.google.com"
    patchType: "engine"
  -
    pid: "patch_009_21"
    patchTag: "Google Scholar"
    domain: "scholar.google.com"
    patchType: "SERP"
  -
    pid: "patch_009_22"
    patchTag: "ScienceDirect"
    domain: "sciencedirect.com"
    patchType: "page"
  -
    pid: "patch_009_23"
    patchTag: "British Medical Journal"
    domain: "bmj.com"
    patchType: "page"
  -
    pid: "patch_009_24"
    patchTag: "ScienceDirect"
    domain: "sciencedirect.com"
    patchType: "page"
  -
    pid: "patch_009_25"
    patchTag: "Scientific Freedom"
    domain: "scientificfreedom.dk"
    patchType: "file"
  -
    pid: "patch_009_26"
    patchTag: "Google Scholar"
    domain: "scholar.google.com"
    patchType: "SERP"
  -
    pid: "patch_009_27"
    patchTag: "Springer Link"
    domain: "springer.com"
    patchType: "page"
  -
    pid: "patch_009_28"
    patchTag: "Google Books"
    domain: "books.google.com"
    patchType: "page"
  -
    pid: "patch_009_29"
    patchTag: "UW-Madison Libraries"
    domain: "library.wisc.edu"
    patchType: "SERP"
  -
    pid: "patch_009_30"
    patchTag: "UW-Madison Libraries"
    domain: "library.wisc.edu"
    patchType: "page"
  -
    pid: "patch_009_31"
    patchTag: "World News Connection"
    domain: "wnc.eastview.com"
    patchType: "page"
  -
    pid: "patch_009_32"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "engine"
  -
    pid: "patch_009_33"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "engine"
  -
    pid: "patch_009_34"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "engine"
  -
    pid: "patch_009_35"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "SERP"
  -
    pid: "patch_009_36"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "page"
  -
    pid: "patch_009_37"
    patchTag: "Newsbank"
    domain: "infoweb.newsbank.com"
    patchType: "engine"
  -
    pid: "patch_009_38"
    patchTag: "Newsbank"
    domain: "infoweb.newsbank.com"
    patchType: "SERP"
---

[[pid: patch_009_1]]
--+-{ anchor_009_1 }-+--

[[pid: patch_009_2]]
--+-{ anchor_009_2 }-+--

[[pid: patch_009_3]]
--+-{ anchor_009_3 }-+--

[[pid: patch_009_4]]
--+-{ anchor_009_4 }-+--

[[pid: patch_009_5]]
--+-{ anchor_009_5 }-+--

[[pid: patch_009_6]]
--+-{ anchor_009_6 }-+--

[[pid: patch_009_7]]
--+-{ anchor_009_7 }-+--

[[pid: patch_009_8]]
--+-{ anchor_009_8 }-+--

[[pid: patch_009_7]]
--+-{ anchor_009_9 }-+--

[[pid: patch_009_9]]
--+-{ anchor_009_10 }-+--

[[pid: patch_009_10]]
--+-{ anchor_009_11 }-+--

[[pid: patch_009_11]]
--+-{ anchor_009_12 }-+--

[[pid: patch_009_12]]
--+-{ anchor_009_13 }-+--

[[pid: patch_009_13]]
--+-{ anchor_009_14 }-+--

[[pid: patch_009_1]]
--+-{ anchor_009_15 }-+--

[[pid: patch_009_14]]
--+-{ anchor_009_16 }-+--

[[pid: patch_009_15]]
--+-{ anchor_009_17 }-+--

[[pid: patch_009_15]]
--+-{ anchor_009_18 }-+--

[[pid: patch_009_16]]
--+-{ anchor_009_19 }-+--

[[pid: patch_009_17]]
--+-{ anchor_009_20 }-+--

[[pid: patch_009_18]]
--+-{ anchor_009_21 }-+--

[[pid: patch_009_16]]
--+-{ anchor_009_22 }-+--

[[pid: patch_009_1]]
--+-{ anchor_009_23 }-+--

[[pid: patch_009_19]]
--+-{ anchor_009_24 }-+--

[[pid: patch_009_20]]
--+-{ anchor_009_25 }-+--

[[pid: patch_009_21]]
--+-{ anchor_009_26 }-+--

[[pid: patch_009_22]]
--+-{ anchor_009_27 }-+--

[[pid: patch_009_23]]
--+-{ anchor_009_28 }-+--

[[pid: patch_009_21]]
--+-{ anchor_009_29 }-+--

[[pid: patch_009_22]]
--+-{ anchor_009_30 }-+--

[[pid: patch_009_23]]
--+-{ anchor_009_31 }-+--

[[pid: patch_009_24]]
--+-{ anchor_009_32 }-+--

[[pid: patch_009_23]]
--+-{ anchor_009_33 }-+--

[[pid: patch_009_25]]
--+-{ anchor_009_34 }-+--

[[pid: patch_009_23]]
--+-{ anchor_009_35 }-+--

[[pid: patch_009_26]]
--+-{ anchor_009_36 }-+--

[[pid: patch_009_27]]
--+-{ anchor_009_37 }-+--

[[pid: patch_009_28]]
--+-{ anchor_009_38 }-+--

[[pid: patch_009_1]]
--+-{ anchor_009_39 }-+--

[[pid: patch_009_2]]
--+-{ anchor_009_40 }-+--

[[pid: patch_009_29]]
--+-{ anchor_009_41 }-+--

[[pid: patch_009_30]]
--+-{ anchor_009_42 }-+--

[[pid: patch_009_31]]
--+-{ anchor_009_43 }-+--

[[pid: patch_009_29]]
--+-{ anchor_009_44 }-+--

[[pid: patch_009_32]]
--+-{ anchor_009_45 }-+--

[[pid: patch_009_33]]
--+-{ anchor_009_46 }-+--

[[pid: patch_009_34]]
--+-{ anchor_009_47 }-+--

[[pid: patch_009_35]]
--+-{ anchor_009_48 }-+--

[[pid: patch_009_36]]
--+-{ anchor_009_49 }-+--

[[pid: patch_009_37]]
--+-{ anchor_009_50 }-+--

[[pid: patch_009_38]]
--+-{ anchor_009_51 }-+--

