[[sourceId=source002]]
[[streamId=SC]]
[[cid=case002]]

---
ROCK_attributes:
  -
    pid: "patch_002_1"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
  -
    pid: "patch_002_2"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
  -
    pid: "patch_002_3"
    patchTag: "United States Agency for International Development"
    domain: "usaid.gov"
    patchType: "file"
  -
    pid: "patch_002_4"
    patchTag: "U.S. Department of Defense"
    domain: "media.defense.gov"
    patchType: "file"
  -
    pid: "patch_002_5"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
  -
    pid: "patch_002_6"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
  -
    pid: "patch_002_7"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
  -
    pid: "patch_002_8"
    patchTag: "U. S. Department of Health & Human Services"
    domain: "hhs.gov"
    patchType: "page"
  -
    pid: "patch_002_9"
    patchTag: "The White House"
    domain: "whitehouse.gov"
    patchType: "page"
  -
    pid: "patch_002_10"
    patchTag: "Wall Street Journal"
    domain: "wsj.com"
    patchType: "page"
  -
    pid: "patch_002_11"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
  -
    pid: "patch_002_12"
    patchTag: "WebMD"
    domain: "webmd.com"
    patchType: "page"
  -
    pid: "patch_002_14"
    patchTag: "National Library of Medicine"
    domain: "ncbi.nlm.nih.gov"
    patchType: "page"
  -
    pid: "patch_002_15"
    patchTag: "U. S. Department of Health & Human Services"
    domain: "hhs.gov"
    patchType: "SERP"
  -
    pid: "patch_002_16"
    patchTag: "Centers for Disease Control and Prevention"
    domain: "cdc.gov"
    patchType: "page"
  -
    pid: "patch_002_17"
    patchTag: "Centers for Disease Control and Prevention"
    domain: "cdc.gov"
    patchType: "page"
  -
    pid: "patch_002_18"
    patchTag: "Centers for Disease Control and Prevention"
    domain: "cdc.gov"
    patchType: "page"
  -
    pid: "patch_002_19"
    patchTag: "Centers for Disease Control and Prevention"
    domain: "cdc.gov"
    patchType: "page"
  -
    pid: "patch_002_20"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
  -
    pid: "patch_002_21"
    patchTag: "Science"
    domain: "science.org"
    patchType: "page"
---

[[pid: patch_002_1]]
--+-{ anchor_002_1 }-+--

[[pid: patch_002_2]]
--+-{ anchor_002_2 }-+--

[[pid: patch_002_3]]
--+-{ anchor_002_3 }-+--

[[pid: patch_002_4]]
--+-{ anchor_002_4 }-+--

[[pid: patch_002_2]]
--+-{ anchor_002_5 }-+--

[[pid: patch_002_5]]
--+-{ anchor_002_6 }-+--

[[pid: patch_002_6]]
--+-{ anchor_002_7 }-+--

[[pid: patch_002_7]]
--+-{ anchor_002_8 }-+--

[[pid: patch_002_8]]
--+-{ anchor_002_9 }-+--

[[pid: patch_002_9]]
--+-{ anchor_002_10 }-+--

[[pid: patch_002_10]]
--+-{ anchor_002_11 }-+--

[[pid: patch_002_7]]
--+-{ anchor_002_12 }-+--

[[pid: patch_002_11]]
--+-{ anchor_002_13 }-+--

[[pid: patch_002_12]]
--+-{ anchor_002_14 }-+--

[[pid: patch_002_14]]
--+-{ anchor_002_15 }-+--

[[pid: patch_002_8]]
--+-{ anchor_002_16 }-+--

[[pid: patch_002_15]]
--+-{ anchor_002_17 }-+--

[[pid: patch_002_16]]
--+-{ anchor_002_18 }-+--

[[pid: patch_002_17]]
--+-{ anchor_002_19 }-+--

[[pid: patch_002_18]]
--+-{ anchor_002_20 }-+--

[[pid: patch_002_19]]
--+-{ anchor_002_21 }-+--

[[pid: patch_002_4]]
--+-{ anchor_002_22 }-+--

[[pid: patch_002_3]]
--+-{ anchor_002_23 }-+--

[[pid: patch_002_19]]
--+-{ anchor_002_24 }-+--

[[pid: patch_002_11]]
--+-{ anchor_002_25 }-+--

[[pid: patch_002_20]]
--+-{ anchor_002_26 }-+--

[[pid: patch_002_21]]
--+-{ anchor_002_27 }-+--

