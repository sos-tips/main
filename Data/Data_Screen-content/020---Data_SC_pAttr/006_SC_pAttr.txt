[[sourceId=source006]]
[[streamId=SC]]
[[cid=case006]]

---
ROCK_attributes:
  -
    pid: "patch_006_1"
    patchTag: "UW-Madison Libraries"
    domain: "library.wisc.edu"
    patchType: "engine"
  -
    pid: "patch_006_2"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
  -
    pid: "patch_006_3"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
  -
    pid: "patch_006_4"
    patchTag: "National Institute of Allergy and Infectious Diseases"
    domain: "niaid.nih.gov"
    patchType: "page"
  -
    pid: "patch_006_5"
    patchTag: "National Institute of Allergy and Infectious Diseases"
    domain: "niaid.nih.gov"
    patchType: "page"
  -
    pid: "patch_006_6"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
  -
    pid: "patch_006_7"
    patchTag: "Science"
    domain: "science.org"
    patchType: "page"
  -
    pid: "patch_006_8"
    patchTag: "World Health Organization"
    domain: "who.int"
    patchType: "page"
  -
    pid: "patch_006_9"
    patchTag: "World Health Organization"
    domain: "cdn.who.int"
    patchType: "file"
  -
    pid: "patch_006_10"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
---

[[pid: patch_006_1]]
--+-{ anchor_006_1 }-+--

[[pid: patch_006_2]]
--+-{ anchor_006_2 }-+--

[[pid: patch_006_3]]
--+-{ anchor_006_3 }-+--

[[pid: patch_006_4]]
--+-{ anchor_006_4 }-+--

[[pid: patch_006_5]]
--+-{ anchor_006_5 }-+--

[[pid: patch_006_2]]
--+-{ anchor_006_6 }-+--

[[pid: patch_006_6]]
--+-{ anchor_006_7 }-+--

[[pid: patch_006_7]]
--+-{ anchor_006_8 }-+--

[[pid: patch_006_8]]
--+-{ anchor_006_9 }-+--

[[pid: patch_006_9]]
--+-{ anchor_006_10 }-+--

[[pid: patch_006_1]]
--+-{ anchor_006_11 }-+--

[[pid: patch_006_10]]
--+-{ anchor_006_12 }-+--

[[pid: patch_006_9]]
--+-{ anchor_006_13 }-+--

