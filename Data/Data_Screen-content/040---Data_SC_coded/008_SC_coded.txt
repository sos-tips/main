[[sourceId=source008]]
[[streamId=SC]]
[[cid=case008]]

---
ROCK_attributes:
  -
    pid: "patch_008_1"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
    totalTime: "00:24"
  -
    pid: "patch_008_2"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:24"
  -
    pid: "patch_008_3"
    patchTag: "The New York Times"
    domain: "nytimes.com"
    patchType: "page"
    totalTime: "02:34"
  -
    pid: "patch_008_4"
    patchTag: "UW-Madison Libraries"
    domain: "staff.library.wisc.edu"
    patchType: "page"
    totalTime: "00:03"
  -
    pid: "patch_008_5"
    patchTag: "UW-Madison Libraries"
    domain: "library.wisc.edu"
    patchType: "engine"
    totalTime: "00:16"
  -
    pid: "patch_008_6"
    patchTag: "Director of National Intelligence"
    domain: "dni.gov"
    patchType: "file"
    totalTime: "01:15"
  -
    pid: "patch_008_7"
    patchTag: "Director of National Intelligence"
    domain: "dni.gov"
    patchType: "page"
    totalTime: "00:10"
  -
    pid: "patch_008_8"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:19"
  -
    pid: "patch_008_9"
    patchTag: "Vox"
    domain: "vox.com"
    patchType: "page"
    totalTime: "01:51"
  -
    pid: "patch_008_10"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "SERP"
    totalTime: "00:23"
  -
    pid: "patch_008_11"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "SERP"
    totalTime: "01:52"
  -
    pid: "patch_008_12"
    patchTag: "UW-Madison Libraries"
    domain: "search.library.wisc.edu"
    patchType: "page"
    totalTime: "00:09"
  -
    pid: "patch_008_13"
    patchTag: "Science"
    domain: "science.org"
    patchType: "file"
    totalTime: "00:18"
---

[[pid: patch_008_1]]
--+-{ anchor_008_1 }-+--

[[pid: patch_008_2]]
--+-{ anchor_008_2 }-+--

[[pid: patch_008_3]]
--+-{ anchor_008_3 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Refs_pres]] [[SC>APPR>Ads_pres]]

[[pid: patch_008_1]]
--+-{ anchor_008_4 }-+--

[[pid: patch_008_4]]
--+-{ anchor_008_5 }-+--
[[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Date_abs]]

[[pid: patch_008_5]]
--+-{ anchor_008_6 }-+--

[[pid: patch_008_3]]
--+-{ anchor_008_7 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Refs_pres]] [[SC>APPR>Ads_pres]]

[[pid: patch_008_2]]
--+-{ anchor_008_8 }-+--

[[pid: patch_008_6]]
--+-{ anchor_008_9 }-+--
[[SC>APPR>URL_pres]] [[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>DOI_abs]] [[SC>APPR>Refs_abs]]

[[pid: patch_008_7]]
--+-{ anchor_008_10 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_008_2]]
--+-{ anchor_008_11 }-+--

[[pid: patch_008_8]]
--+-{ anchor_008_12 }-+--

[[pid: patch_008_9]]
--+-{ anchor_008_13 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_pres]] [[SC>APPR>Refs_pres]]

[[pid: patch_008_10]]
--+-{ anchor_008_14 }-+--

[[pid: patch_008_11]]
--+-{ anchor_008_15 }-+--

[[pid: patch_008_12]]
--+-{ anchor_008_16 }-+--
[[SC>APPR>URL_pres]] [[SC>APPR>DOI_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>SciMet_abs]]

[[pid: patch_008_13]]
--+-{ anchor_008_17 }-+--

[[pid: patch_008_12]]
--+-{ anchor_008_18 }-+--
[[SC>APPR>URL_pres]] [[SC>APPR>DOI_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>SciMet_abs]]

[[pid: patch_008_11]]
--+-{ anchor_008_19 }-+--
[[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>DOI_pres]] [[SC>APPR>Date_pres]]
