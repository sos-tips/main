[[sourceId=source018]]
[[streamId=SC]]
[[cid=case018]]


---
ROCK_attributes:
  -
    pid: "patch_018_1"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "engine"
    totalTime: "00:47"
  -
    pid: "patch_018_2"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:06"
  -
    pid: "patch_018_3"
    patchTag: "National Institute of Allergy and Infectious Diseases"
    domain: "niaid.nih.gov"
    patchType: "page"
    totalTime: "01:06"
  -
    pid: "patch_018_4"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:16"
  -
    pid: "patch_018_5"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:14"
  -
    pid: "patch_018_6"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:18"
  -
    pid: "patch_018_7"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:06"
  -
    pid: "patch_018_8"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:03"
  -
    pid: "patch_018_9"
    patchTag: "Baidu"
    domain: "baidu.com"
    patchType: "engine"
    totalTime: "00:03"
  -
    pid: "patch_018_10"
    patchTag: "Baidu"
    domain: "baidu.com"
    patchType: "SERP"
    totalTime: "00:33"
  -
    pid: "patch_018_12"
    patchTag: "Financial Times"
    domain: "ft.com"
    patchType: "page"
    totalTime: "00:04"
  -
    pid: "patch_018_13"
    patchTag: "Centers for Disease Control and Prevention"
    domain: "cdc.gov"
    patchType: "page"
    totalTime: "00:39"
  -
    pid: "patch_018_14"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:11"
  -
    pid: "patch_018_15"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:26"
  -
    pid: "patch_018_16"
    patchTag: "CNN"
    domain: "cnn.com"
    patchType: "SERP"
    totalTime: "00:29"
  -
    pid: "patch_018_17"
    patchTag: "CNN"
    domain: "cnn.com"
    patchType: "SERP"
    totalTime: "00:23"
  -
    pid: "patch_018_18"
    patchTag: "Director of National Intelligence"
    domain: "dni.gov"
    patchType: "file"
    totalTime: "01:00"
  -
    pid: "patch_018_19"
    patchTag: "Director of National Intelligence"
    domain: "dni.gov"
    patchType: "page"
    totalTime: "00:04"
  -
    pid: "patch_018_20"
    patchTag: "Science"
    domain: "science.org"
    patchType: "page"
    totalTime: "00:49"
  -
    pid: "patch_018_21"
    patchTag: "Council on Foreign Relations"
    domain: "cfr.org"
    patchType: "page"
    totalTime: "00:35"
  -
    pid: "patch_018_22"
    patchTag: "Bulletin of the Atomic Scientists"
    domain: "thebulletin.org"
    patchType: "page"
    totalTime: "00:07"
  -
    pid: "patch_018_23"
    patchTag: "Reddit"
    domain: "reddit.com"
    patchType: "page"
    totalTime: "00:13"
  -
    pid: "patch_018_24"
    patchTag: "The New Reddit Journal of Science"
    domain: "reddit.com"
    patchType: "page"
    totalTime: "00:02"
  -
    pid: "patch_018_25"
    patchTag: "Google All"
    domain: "google.com"
    patchType: "SERP"
    totalTime: "00:02"
  -
    pid: "patch_018_26"
    patchTag: "Wikipedia"
    domain: "wikipedia.org"
    patchType: "page"
    totalTime: "01:08"
---


[[pid: patch_018_1]]
--+-{ anchor_018_1 }-+--

[[pid: patch_018_2]]
--+-{ anchor_018_2 }-+--

[[pid: patch_018_3]]
--+-{ anchor_018_3 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_018_1]]
--+-{ anchor_018_4 }-+--

[[pid: patch_018_4]]
--+-{ anchor_018_5 }-+--

[[pid: patch_018_5]]
--+-{ anchor_018_6 }-+--

[[pid: patch_018_6]]
--+-{ anchor_018_7 }-+--

[[pid: patch_018_7]]
--+-{ anchor_018_8 }-+--

[[pid: patch_018_8]]
--+-{ anchor_018_9 }-+--

[[pid: patch_018_9]]
--+-{ anchor_018_10 }-+--

[[pid: patch_018_10]]
--+-{ anchor_018_11 }-+--

[[pid: patch_018_3]]
--+-{ anchor_018_12 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_018_12]]
--+-{ anchor_018_13 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_abs]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_018_13]]
--+-{ anchor_018_14 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Refs_abs]]

[[pid: patch_018_2]]
--+-{ anchor_018_15 }-+--

[[pid: patch_018_14]]
--+-{ anchor_018_16 }-+--

[[pid: patch_018_15]]
--+-{ anchor_018_17 }-+--

[[pid: patch_018_16]]
--+-{ anchor_018_18 }-+--

[[pid: patch_018_17]]
--+-{ anchor_018_19 }-+--

[[pid: patch_018_18]]
--+-{ anchor_018_20 }-+--
[[SC>APPR>DOI_abs]] [[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_abs]]

[[pid: patch_018_15]]
--+-{ anchor_018_21 }-+--

[[pid: patch_018_19]]
--+-{ anchor_018_22 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_abs]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_018_18]]
--+-{ anchor_018_23 }-+--
[[SC>APPR>DOI_abs]] [[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_abs]]

[[pid: patch_018_3]]
--+-{ anchor_018_24 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_018_10]]
--+-{ anchor_018_25 }-+--

[[pid: patch_018_13]]
--+-{ anchor_018_26 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Refs_abs]]

[[pid: patch_018_20]]
--+-{ anchor_018_27 }-+--
[[SC>APPR>URL_pres]] [[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>Refs_pres]] [[SC>APPR>DOI_pres]] [[SC>APPR>Ads_abs]]

[[pid: patch_018_21]]
--+-{ anchor_018_28 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>URL_pres]] 
[[SC>APPR>Ads_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_pres]]

[[pid: patch_018_22]]
--+-{ anchor_018_29 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Refs_abs]]

[[pid: patch_018_23]]
--+-{ anchor_018_30 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_pres]]

[[pid: patch_018_24]]
--+-{ anchor_018_31 }-+--
[[SC>APPR>Date_pres]] [[SC>APPR>Author_pres]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_pres]]

[[pid: patch_018_13]]
--+-{ anchor_018_32 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]] [[SC>APPR>Refs_abs]]

[[pid: patch_018_10]]
--+-{ anchor_018_33 }-+--

[[pid: patch_018_25]]
--+-{ anchor_018_34 }-+--

[[pid: patch_018_26]]
--+-{ anchor_018_35 }-+--
[[SC>APPR>Date_abs]] [[SC>APPR>Author_abs]] [[SC>APPR>AuthAffil_abs]] [[SC>APPR>Refs_pres]] [[SC>APPR>URL_pres]] [[SC>APPR>Ads_abs]]
