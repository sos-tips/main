[[sourceId=source019]]
[[streamId=HCI]]
[[cid=case019]]

[[pid: patch_019_1]]
--+-{ anchor_019_1 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_019_1"
patchType: "engine"
start: "00:00"
end: "00:20"
HCI1: "type | Origin of coronavirus | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_019_2]]
--+-{ anchor_019_2 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_019_2"
patchType: "SERP"
start: "00:21"
end: "01:05"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | Wuhan market was epicentre of pandemic's start, studies... | result 5 title"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"
HCI6: "click | Origins of Coronaviruses / NIH | result 1 title"
HCI7: "scroll | down | page"
HCI8: "scroll | up | page"
HCI9: "scroll | down | page"

[[pid: patch_019_3]]
--+-{ anchor_019_3 }-+--
patchTag: "Nature"
domain: "nature.com"
pid: "patch_019_3"
patchType: "page"
start: "01:06"
end: "01:15"
HCI1: "scroll | down | page"

[[pid: patch_019_4]]
--+-{ anchor_019_4 }-+--
patchTag: "National Institute of Allergy and Infectious Diseases"
domain: "niaid.nih.gov"
pid: "patch_019_4"
patchType: "page"
start: "01:16"
end: "01:32"
HCI1: "highlight | civets | page"

[[pid: patch_019_1]]
--+-{ anchor_019_5 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_019_1"
patchType: "engine"
start: "01:33"
end: "01:35"
HCI1: "type | civets | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_019_5]]
--+-{ anchor_019_6 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_019_5"
patchType: "SERP"
start: "01:36"
end: "01:46"

[[pid: patch_019_4]]
--+-{ anchor_019_7 }-+--
patchTag: "National Institute of Allergy and Infectious Diseases"
domain: "niaid.nih.gov"
pid: "patch_019_4"
patchType: "page"
start: "01:47"
end: "02:08"
HCI1: "scroll | down | page"

[[pid: patch_019_2]]
--+-{ anchor_019_8 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_019_2"
patchType: "SERP"
start: "02:11"
end: "02:20"
HCI1: "scroll | up | page"
HCI2: "type | origin of covid-19 | search bar"
HCI3: "type | enter | search bar"

[[pid: patch_019_6]]
--+-{ anchor_019_9 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_019_6"
patchType: "SERP"
start: "02:12"
end: "02:39"
HCI1: "scroll | down | page"
HCI2: "click | Origins of SARS-CoV-2 / John Hopkins | result 1 title"
HCI3: "scroll | down | page"

[[pid: patch_019_7]]
--+-{ anchor_019_10 }-+--
patchTag: "John Hopkins Bloomberg School of Public Health"
domain: "publichealth.jhu.edu.com"
pid: "patch_019_7"
patchType: "page"
start: "02:40"
end: "03:43"
HCI1: "scroll | down | page"
HCI2: "highlight | knell | page"

[[pid: patch_019_1]]
--+-{ anchor_019_11 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_019_1"
patchType: "engine"
start: "03:44"
end: "03:46"
HCI1: "type | knell | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_019_8]]
--+-{ anchor_019_12 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_019_8"
patchType: "SERP"
start: "03:47"
end: "03:49"

[[pid: patch_019_7]]
--+-{ anchor_019_13 }-+--
patchTag: "John Hopkins Bloomberg School of Public Health"
domain: "publichealth.jhu.edu.com"
pid: "patch_019_7"
patchType: "page"
start: "03:50"
end: "05:40"
HCI1: "scroll | down | page"
HCI2: "highlight | geospatial | page"
HCI3: "scroll | down | page"
HCI4: "highlight | spillover | page"
HCI5: "scroll | down | page"
HCI6: "highlight | Raccoon dogs | page"

[[pid: patch_019_1]]
--+-{ anchor_019_14 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_019_1"
patchType: "engine"
start: "05:41"
end: "05:43"
HCI1: "type | Racoon dogs | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_019_9]]
--+-{ anchor_019_15 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_019_9"
patchType: "SERP"
start: "05:44"
end: "05:46"

[[pid: patch_019_7]]
--+-{ anchor_019_16 }-+--
patchTag: "John Hopkins Bloomberg School of Public Health"
domain: "publichealth.jhu.edu.com"
pid: "patch_019_7"
patchType: "page"
start: "05:47"
end: "06:54"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "click | The Contested Origin of SARS-CoV-2 | hyperlink"

[[pid: patch_019_10]]
--+-{ anchor_019_17 }-+--
patchTag: "Taylor Francis Online"
domain: "tandfonline.com"
pid: "patch_019_10"
patchType: "page"
start: "06:56"
end: "07:04"
HCI1: "click | View PDF | hyperlink"

[[pid: patch_019_11]]
--+-{ anchor_019_18 }-+--
patchTag: "Taylor Francis Online"
domain: "tandfonline.com"
pid: "patch_019_11"
patchType: "file"
start: "07:05"
end: "07:29"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_019_10]]
--+-{ anchor_019_19 }-+--
patchTag: "Taylor Francis Online"
domain: "tandfonline.com"
pid: "patch_019_10"
patchType: "page"
start: "06:56"
end: "10:10"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "highlight | dispassionate science-based discourse | page"
HCI5: "scroll | down | page"
HCI6: "hover | 7 | hyperlink"
