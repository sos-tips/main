[[sourceId=source010]]
[[streamId=HCI]]
[[cid=case010]]

[[pid: patch_010_1]]
--+-{ anchor_010_1 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_010_1"
patchType: "engine"
start: "00:00"
end: "00:08"
HCI1: "type | covid origin | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_010_2]]
--+-{ anchor_010_2 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_010_2"
patchType: "SERP"
start: "00:09"
end: "00:34"
HCI1: "scroll | down | page
HCI2: "click | Scientists 'strongly condemn' rumors and conspiracy theories... | result 1 title"
HCI3: "scroll |down | page"
HCI4: "click | Compelling new evidence tracks COVID's origin to Wuhan... | result 2 title"
HCI5: "click | Origins of Coronaviruses / NIH' | result 3 title"
HCI6: "scroll | down | page"
HCI7: "click | Covid-19 origins: New studies agree that animal sold... - CNN | result 4 title"
HCI8: "scroll | up | page"
HCI9: "type | covid lab leak | search bar"
HCI10: "type | enter | search bar"

[[pid: patch_010_3]]
--+-{ anchor_010_3 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_010_3"
patchType: "SERP"
start: "00:35"
end: "00:48"
HCI1: "scroll | down | page"
HCI2: "click |From 'open minded' to 'underwhelming,' mixed reactions greet... | result 2 title"
HCI3: "scroll | down | page"
HCI4: "click | COVID-19 lab leak theory - Wikipedia | result 5 title"

[[pid: patch_010_4]]
--+-{ anchor_010_4 }-+--
patchTag: "Wikipedia"
domain: "wikipedia.org"
pid: "patch_010_4"
patchType: "page"
start: "00:49"
end: "02:43"
HCI1: "scroll | down | page"
HCI2: "click | SARS-related coronaviruses | hyperlink"
HCI3: "scroll | down | page"
HCI4: "hover | natural reservoir | hyperlink"
HCI5: "scroll | down | page"
HCI6: "hover | betacoronaviruses | hyperlink"
HCI7: "scroll | down | page"
HCI8: "click | photo| page"

[[pid: patch_010_5]]
--+-{ anchor_010_5 }-+--
patchTag: "Wikipedia"
domain: "wikipedia.org"
pid: "patch_010_5"
patchType: "file"
start: "02:44"
end: "02:52"

[[pid: patch_010_4]]
--+-{ anchor_010_6 }-+--
patchTag: "Wikipedia"
domain: "wikipedia.org"
pid: "patch_010_4"
patchType: "page"
start: "02:53"
end: "03:08"
HCI1: "scroll | down | page"
HCI2: "click | Laboratory leak incidents | hyperlink

[[pid: patch_010_6]]
--+-{ anchor_010_7 }-+--
patchTag: "Wikipedia"
domain: "wikipedia.org"
pid: "patch_010_6"
patchType: "page"
start: "03:09"
end: "03:53"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_010_7]]
--+-{ anchor_010_8 }-+--
patchTag: "Wikipedia"
domain: "wikipedia.org"
pid: "patch_010_7"
patchType: "page"
start: "03:54"
end: "03:59"

[[pid: patch_010_4]]
--+-{ anchor_010_9 }-+--
patchTag: "Wikipedia"
domain: "wikipedia.org"
pid: "patch_010_4"
patchType: "page"
start: "04:00"
end: "05:24"
HCI1: "scroll | down | page"
HCI2: "click | Gain-of-function research & COVID-19 pandemic | hyperlink"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"

[[pid: patch_010_8]]
--+-{ anchor_010_10 }-+--
patchTag: "Wikipedia"
domain: "wikipedia.org"
pid: "patch_010_8"
patchType: "page"
start: "05:25"
end: "05:49"

[[pid: patch_010_9]]
--+-{ anchor_010_11 }-+--
patchTag: "Wikipedia"
domain: "wikipedia.org"
pid: "patch_010_9"
patchType: "page"
start: "05:50"
end: "05:55"

[[pid: patch_010_10]]
--+-{ anchor_010_12 }-+--
patchTag: "Science"
domain: "science.org"
pid: "patch_010_10"
patchType: "page"
start: "05:56"
end: "06:24"
HCI1: "scroll | down | page"
HCI2: "click | published online | hyperlink"
HCI3: "scroll | down | page"

[[pid: patch_010_11]]
--+-{ anchor_010_13 }-+--
patchTag: "National Institute of Allergy and Infectious Diseases"
domain: "niaid.nih.gov"
pid: "patch_010_11"
patchType: "page"
start: "06:28"
end: "06:41"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | SARS-COV-2 and NIAID-Supported Bat Coronavirus Research | hyperlink"

[[pid: patch_010_12]]
--+-{ anchor_010_14 }-+--
patchTag: "CNN"
domain: "cnn.com"
pid: "patch_010_12"
patchType: "page"
start: "06:42"
end: "08:41"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "highlight | Wertheim | page"

[[pid: patch_010_13]]
--+-{ anchor_010_15 }-+--
patchTag: "Science"
domain: "science.org"
pid: "patch_010_13"
patchType: "page"
start: "08:42"
end: "10:03"
HCI1: "scroll | down | page"
