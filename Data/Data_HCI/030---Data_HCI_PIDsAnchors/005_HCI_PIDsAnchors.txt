[[sourceId=source005]]
[[streamId=HCI]]
[[cid=case005]]

[[pid: patch_005_1]]
--+-{ anchor_005_1 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_005_1"
patchType:"engine"
start: "00:02" 
end: "00:05"
HCI1: "type | pubmed | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_005_2]]
--+-{ anchor_005_2 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_005_2"
patchType: "SERP"
start: "00:06"
end: "00:08"
HCI1: "click | PubMed | result 1 title"

[[pid: patch_005_3]]
--+-{ anchor_005_3 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_3"
patchType: "engine"
start: "00:09"
end: "00:16"
HCI1: "type | the origin of sars-cov-2 | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_005_4]]
--+-{ anchor_005_4 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_4"
patchType: "SERP"
start: "00:17"
end: "00:24"
HCI1: "scroll | down | page"
HCI2: "click | More filters | filter"

[[pid: patch_005_5]]
--+-{ anchor_005_5 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_5"
patchType: "SERP"
start: "00:25"
end: "00:31"
HCI1: "scroll | down | page"
HCI2: "click | Filter | filter"
HCI3: "click | Etiology | filter"

[[pid: patch_005_6]]
--+-{ anchor_005_6 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_6"
patchType: "SERP"
start: "00:32"
end: "00:36"
HCI1: "scroll | down | page"
HCI2: "click | Filter | filter"

[[pid: patch_005_1]]
--+-{ anchor_005_7 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_005_1"
patchType: "engine"
start: "00:37"
end: "00:39"
HCI1: "type | etiology definition | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_005_7]]
--+-{ anchor_005_8 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_005_7"
patchType: "SERP"
start: "00:40"
end: "00:42"

[[pid: patch_005_6]]
--+-{ anchor_005_9 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_6"
patchType: "SERP"
start: "00:43"
end: "00:52"
HCI1: "scroll | down | page"
HCI2: "click | See all results in PubMed (776) | hyperlink" 

[[pid: patch_005_8]]
--+-{ anchor_005_10 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_8"
patchType: "SERP"
start: "00:53"
end: "01:17"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "type | (sars-cov-2) AND (Etiology/Broad[filter]) | search bar"
HCI4: "type | enter | search bar"

[[pid: patch_005_9]]
--+-{ anchor_005_11 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_9"
patchType: "SERP"
start: "01:18"
end: "01:33"
HCI1: "scroll | down | page"
HCI2: "click | Meta-Analysis | filter"

[[pid: patch_005_10]]
--+-{ anchor_005_12 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_10"
patchType: "SERP"
start: "01:34"
end: "01:40"
HCI1: "scroll | down | page"
HCI2: "click | Randomized Controlled Trial | filter"

[[pid: patch_005_11]]
--+-{ anchor_005_13 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_11"
patchType: "SERP"
start: "01:41"
end: "01:45"
HCI1: "click | 2004-2020 | filter"

[[pid: patch_005_12]]
--+-{ anchor_005_14 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_12"
patchType: "SERP"
start: "01:46"
end: "01:51"
HCI1: "scroll | down | page"
HCI2: "click | COVID-19: Discovery, diagnostics and drug development | result 3 title"

[[pid: patch_005_13]]
--+-{ anchor_005_15 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_13"
patchType: "page"
start: "01:52"
end: "02:26"
HCI1: "scroll | down | page"

[[pid: patch_005_12]]
--+-{ anchor_005_16 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_12"
patchType: "SERP"
start: "02:27"
end: "02:37"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | 2004-2022 | filter"

[[pid: patch_005_14]]
--+-{ anchor_005_17 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_14"
patchType: "SERP"
start: "02:38"
end: "02:51"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "type | (sars-cov-2) AND (Etiology/Broad[filter]) AND (wuhan) | search bar"
HCI4: "type | enter | search bar"

[[pid: patch_005_15]]
--+-{ anchor_005_18 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_15"
patchType: "SERP"
start: "02:52"
end: "03:15"
HCI1: "scroll | down | page"
HCI2: "click | Infection-enhancing anti-SARS-CoV-2 antibodies recognize both the original Wuhan/D614G strain and Delta variants. A potential risk for mass vaccination? | result 9 title"

[[pid: patch_005_16]]
--+-{ anchor_005_19 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_16"
patchType: "page"
start: "03:16"
end: "03:29"
HCI1: "highlight | Wuhan/D614G strain | page"
HCI2: "type | Ctrl + C | page"
HCI3: "highlight | wuhan | search bar"
HCI4: "type | Ctrl + V | search bar"

[[pid: patch_005_15]]
--+-{ anchor_005_20 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_15"
patchType: "SERP"
start: "03:30"
end: "03:36"
HCI1: "scroll | up | page"
HCI2: "highlight | wuhan | search bar"
HCI3: "type | Ctrl + V | search bar"
HCI4: "type | enter | search bar"

[[pid: patch_005_17]]
--+-{ anchor_005_21 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_17"
patchType: "page"
start: "03:37"
end: "03:40"
HCI1: "type | (sars-cov-2) AND (Etiology/Broad[filter]) AND (614G strain) | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_005_18]]
--+-{ anchor_005_22 }-+--
patchTag: "PubMed"
domain: "pubmed.ncbi.nlm.nih.gov"
pid: "patch_005_18"
patchType: "page"
start: "03:41"
end: "04:05"
HCI1: "scroll | down | page"

[[pid: patch_005_1]]
--+-{ anchor_005_23 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_005_1"
patchType:"engine"
start: "04:06" 
end: "04:15"
HCI1: "type | world health organization | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_005_19]]
--+-{ anchor_005_24 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_005_19"
patchType:"SERP"
start: "04:16" 
end: "04:20"
HCI1: "click | Coronavirus disease (COVID-19) | result 1 subtitle"

[[pid: patch_005_20]]
--+-{ anchor_005_25 }-+--
patchTag: "World Health Organization"
domain: "who.int"
pid: "patch_005_20"
patchType:"page"
start: "04:21" 
end: "04:29"
HCI1: "scroll | down | page"
HCI2: "click | Origins of the SARS-CoV-2 virus | menu"

[[pid: patch_005_21]]
--+-{ anchor_005_26 }-+--
patchTag: "World Health Organization"
domain: "who.int"
pid: "patch_005_21"
patchType:"page"
start: "04:30" 
end: "04:35"
HCI1: "scroll | down | page"
HCI2: "click | Read More | hyperlink"

[[pid: patch_005_22]]
--+-{ anchor_005_27 }-+--
patchTag: "World Health Organization"
domain: "who.int"
pid: "patch_005_22"
patchType:"page"
start: "04:36" 
end: "04:56"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | Download (2.3 MB) | hyperlink"

[[pid: patch_005_23]]
--+-{ anchor_005_28 }-+--
patchTag: "World Health Organization"
domain: "who.int"
pid: "patch_005_23"
patchType:"file"
start: "04:57" 
end: "10:03"
HCI1: "scroll | down | page"
HCI2: "click | zoom in | page"
HCI3: "scroll | right | page"
HCI4: "scroll | down | page"

