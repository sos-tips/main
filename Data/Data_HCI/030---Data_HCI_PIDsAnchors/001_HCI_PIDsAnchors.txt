[[sourceId=source001]]
[[streamId=HCI]]
[[cid=case001]]

[[pid: patch_001_1]]
--+-{ anchor_001_1 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_001_1"
patchType: "engine"
start: "00:00"
end: "00:14"
HCI1: "type | scholar.google.com | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_1]]
--+-{ anchor_001_2 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_001_1"
patchType: "engine"
start: "00:15"
end: "00:17"
HCI1: "type | sjr | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_2]]
--+-{ anchor_001_3 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_001_2"
patchType: "SERP"
start: "00:18"
end: "00:22"
HCI1: "click | Scimago Journal & Country Rank | result 1 title"

[[pid: patch_001_3]]
--+-{ anchor_001_4 }-+--
patchTag: "Google Scholar"
domain: "scholar.google.com"
pid: "patch_001_3"
patchType: "engine"
start: "00:23"
end: "00:59"
HCI1: "type | ('COVID-19' OR 'novel coronavirus' AND 'Wuhan' OR 'Origin') | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_4]]
--+-{ anchor_001_5 }-+--
patchTag: "Google Scholar"
domain: "scholar.google.com"
pid: "patch_001_4"
patchType: "SERP"
start: "01:00"
end: "01:53"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "type | ('COVID-19' OR 'novel coronavirus' AND 'Wuhan' OR 'Origin' AND ('bat')) | search bar"
HCI6: "type | enter | search bar"

[[pid: patch_001_5]]
--+-{ anchor_001_6 }-+--
patchTag: "Google Scholar"
domain: "scholar.google.com"
pid: "patch_001_5"
patchType: "SERP"
start: "01:54"
end: "02:34"
HCI1: "scroll | down | page"
HCI2: "click | The origin of COVID-19 and why it matters | result 7 DirectLink"

[[pid: patch_001_6]]
--+-{ anchor_001_7 }-+--
patchTag: "National Library of Medicine"
domain: "ncbi.nlm.nih.gov"
pid: "patch_001_6"
patchType: "page"
start: "02:35"
end: "02:43"

[[pid: patch_001_7]]
--+-{ anchor_001_8 }-+--
patchTag: "Scimago Journal & Country Rank"
domain: "scimagojr.com"
pid: "patch_001_7"
patchType: "engine"
start: "02:44"
end: "02:52"
HCI1: "type | american journal tropical medicine | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_8]]
--+-{ anchor_001_9 }-+--
patchTag: "Scimago Journal & Country Rank"
domain: "scimagojr.com"
pid: "patch_001_8"
patchType: "SERP"
start: "02:53"
end: "02:56"
HCI1: "click | American Journal of Tropical Medicine and Hygiene | result 1 title"

[[pid: patch_001_9]]
--+-{ anchor_001_10 }-+--
patchTag: "Scimago Journal & Country Rank"
domain: "scimagojr.com"
pid: "patch_001_9"
patchType: "page"
start: "02:57"
end: "03:05"
HCI1: "scroll | down | page"

[[pid: patch_001_6]]
--+-{ anchor_001_11 }-+--
patchTag: "National Library of Medicine"
domain: "ncbi.nlm.nih.gov"
pid: "patch_001_6"
patchType: "page"
start: "03:06"
end: "05:25"
HCI1: "scroll | down | page"

[[pid: patch_001_1]]
--+-{ anchor_001_12 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_001_1"
patchType: "engine"
start: "05:26"
end: "05:29"
HCI1: "type | jspaint.app | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_6]]
--+-{ anchor_001_13 }-+--
patchTag: "National Library of Medicine"
domain: "ncbi.nlm.nih.gov"
pid: "patch_001_6"
patchType: "page"
start: "05:30"
end: "05:33"
HCI1: "highlight | screenshot | page"
HCI2: "type | Ctrl + C | page"

[[pid: patch_001_10]]
--+-{ anchor_001_14 }-+--
patchTag: "JS Paint"
domain: "jspaint.app"
pid: "patch_001_10"
patchType: "page"
start: "05:34"
end: "05:36"
HCI1: "type | Ctrl + V | page"
HCI2: "click | screenshot | page"

[[pid: patch_001_6]]
--+-{ anchor_001_15 }-+--
patchTag: "National Library of Medicine"
domain: "ncbi.nlm.nih.gov"
pid: "patch_001_6"
patchType: "page"
start: "05:37"
end: "06:00"
HCI1: "scroll | down | page"

[[pid: patch_001_5]]
--+-{ anchor_001_16 }-+--
patchTag: "Google Scholar"
domain: "scholar.google.com"
pid: "patch_001_5"
patchType: "SERP"
start: "06:01"
end: "06:03"
HCI1: "click | Cited by | result 7 title"

[[pid: patch_001_11]]
--+-{ anchor_001_17 }-+--
patchTag: "Google Scholar"
domain: "scholar.google.com"
pid: "patch_001_11"
patchType: "SERP"
start: "06:04"
end: "06:35"
HCI1: "click | Emerging pandemic diseases: how we got to COVID-19 | result 1 DirectLink"
HCI2: "scroll | down | page"
HCI3: "SARS-CoV-2, Covid-19, and the debunking of conspiracy theories | result 6 DirectLink"
HCI4: "scroll | down | page"

[[pid: patch_001_12]]
--+-{ anchor_001_18 }-+--
patchTag: "ScienceDirect"
domain: "schiencedirect.com"
pid: "patch_001_12"
patchType: "page"
start: "06:36"
end: "06:54"
HCI1: "scroll | down | page"

[[pid: patch_001_13]]
--+-{ anchor_001_19 }-+--
patchTag: "Wiley"
domain: "onlinelibrary.wiley.com"
pid: "patch_001_13"
patchType: "file"
start: "06:55"
end: "07:19"
HCI1: "click | zoom in | page"
HCI2: "scroll | down | page"
HCI3: "scroll | up | page"
HCI4: "scroll | down | page"
HCI5: "scroll | up | page"
HCI6: "scroll | down | page"
HCI7: "click | http://doi.org/10.1002/rmw.2222 | hyperlink"

[[pid: patch_001_14]]
--+-{ anchor_001_20 }-+--
patchTag: "Wiley"
domain: "onlinelibrary.wiley.com"
pid: "patch_001_14"
patchType: "page"
start: "07:20"
end: "07:28"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_001_9]]
--+-{ anchor_001_21 }-+--
patchTag: "Scimago Journal & Country Rank"
domain: "scimagojr.com"
pid: "patch_001_9"
patchType: "page"
start: "07:29"
end: "07:36"
HCI1: "type | reviews medical virology | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_10]]
--+-{ anchor_001_22 }-+--
patchTag: "Scimago Journal & Country Rank"
domain: "scimagojr.com"
pid: "patch_001_9"
patchType: "page"
start: "07:37"
end: "07:39"
HCI1: "click | Reviews in Medical Virology | result 1 title"

[[pid: patch_001_11]]
--+-{ anchor_001_23 }-+--
patchTag: "Scimago Journal & Country Rank"
domain: "scimagojr.com"
pid: "patch_001_8"
patchType: "page"
start: "07:40"
end: "07:42"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_001_14]]
--+-{ anchor_001_24 }-+--
patchTag: "Wiley"
domain: "onlinelibrary.wiley.com"
pid: "patch_001_14"
patchType: "page"
start: "07:43"
end: "10:02"
HCI1: "scroll | down | page"