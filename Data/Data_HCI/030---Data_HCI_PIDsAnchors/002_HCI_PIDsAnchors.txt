[[sourceId=source002]]
[[streamId=HCI]]
[[cid=case002]]

[[pid: patch_002_1]]
--+-{ anchor_002_1 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_002_1"
patchType: "engine"
start: "00:00"
end: "00:20"
HCI1: "type | Facts about COVID-19 | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_002_2]]
--+-{ anchor_002_2 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_002_2"
patchType: "SERP"
start: "00:21"
end: "00:46"
HCI1: "scroll | down | page"
HCI2: "click | Myths VS Facts on Novel Coronavirus - USAID | result 1 title"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "click | Coronavirus Fact Sheet - Department of Defense | result 5 title"
HCI6: "scroll | down | page"
HCI7: "scroll | up | page"

[[pid: patch_002_3]]
--+-{ anchor_002_3 }-+--
patchTag: "United States Agency for International Development"
domain: "usaid.gov"
pid: "patch_002_3"
patchType: "file"
start: "00:47"
end: "00:58"
HCI1: "scroll | down | page"

[[pid: patch_002_4]]
--+-{ anchor_002_4 }-+--
patchTag: "U.S. Department of Defense"
domain: "media.defense.gov"
pid: "patch_002_4"
patchType: "file"
start: "00:59"
end: "01:08"
HCI1: "scroll | down | page"

[[pid: patch_002_2]]
--+-{ anchor_002_5 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_002_2"
patchType: "SERP"
start: "01:09"
end: "01:17"
HCI1: "type | Origin of Covid-19 | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_002_5]]
--+-{ anchor_002_6 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_002_5"
patchType: "SERP"
start: "01:18"
end: "01:44"
HCI1: "scroll | down | page"
HCI2: "click | WSJ Opinion: Was the Coronavirus 'Lab Leak' Origin Correct? | result 4 title"
HCI3: "scroll | down | page"
HCI4: "click | next page | pagination"

[[pid: patch_002_6]]
--+-{ anchor_002_7 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_002_6"
patchType: "SERP"
start: "01:45"
end: "02:01"
HCI1: "scroll | down | page"
HC12: "scroll | up | page"
HCI3: "type | Origin of Covid-19 / gov | search bar"
HCI4: "type | enter | search bar"

[[pid: patch_002_7]]
--+-{ anchor_002_8 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_002_7"
patchType: "SERP"
start: "02:02"
end: "02:16"
HCI1: "click | FACT SHEET: Strategy to Address the Root Causes of... | result 2 title"
HCI2: "scroll | down | page"
HCI3: "click | Office of pandemics and Emerging Threats (PET) / HHS.gov | result 4 title"

[[pid: patch_002_8]]
--+-{ anchor_002_9 }-+--
patchTag: "U. S. Department of Health & Human Services"
domain: "hhs.gov"
pid: "patch_002_8"
patchType: "page"
start: "02:17"
end: "02:39"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"

[[pid: patch_002_9]]
--+-{ anchor_002_10 }-+--
patchTag: "The White House"
domain: "whitehouse.gov"
pid: "patch_002_9"
patchType: "page"
start: "02:40"
end: "02:44"

[[pid: patch_002_10]]
--+-{ anchor_002_11 }-+--
patchTag: "Wall Street Journal"
domain: "wsj.com"
pid: "patch_002_10"
patchType: "page"
start: "02:50"
end: "02:57"
HCI1: "scroll | down | page"

[[pid: patch_002_7]]
--+-{ anchor_002_12 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_002_7"
patchType: "SERP"
start: "00:59"
end: "03:13"
HCI1: "type | Where did Covid-19 originate? | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_002_11]]
--+-{ anchor_002_13 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_002_11"
patchType: "SERP"
start: "03:14"
end: "03:35"
HCI1: "scroll | down | page"
HCI2: "click | Coronavirus History - WebMD | result 4 title"
HCI3: "click | Did covid-19 came from a lab? - PMC - NCBI | result 8 title"

[[pid: patch_002_12]]
--+-{ anchor_002_14 }-+--
patchTag: "WebMD"
domain: "webmd.com"
pid: "patch_002_12"
patchType: "page"
start: "03:36"
end: "04:33"
HCI1: "scroll | down | page"

[[pid: patch_002_14]]
--+-{ anchor_002_15 }-+--
patchTag: "National Library of Medicine"
domain: "ncbi.nlm.nih.gov"
pid: "patch_002_14"
patchType: "page"
start: "04:34"
end: "05:40"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_002_8]]
--+-{ anchor_002_16 }-+--
patchTag: "U. S. Department of Health & Human Services"
domain: "hhs.gov"
pid: "patch_002_8"
patchType: "page"
start: "05:42"
end: "06:00"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "type | origin of covid 19 | search bar"
HCI4: "type | enter | search bar"

[[pid: patch_002_15]]
--+-{ anchor_002_17 }-+--
patchTag: "U. S. Department of Health & Human Services"
domain: "hhs.gov"
pid: "patch_002_15"
patchType: "SERP"
start: "06:01"
end: "06:11"
HCI1: "click | CDC Coronavirus Disease 2019 (COVID-19) Resources | result 1 title"

[[pid: patch_002_16]]
--+-{ anchor_002_18 }-+--
patchTag: "Centers for Disease Control and Prevention"
domain: "cdc.gov"
pid: "patch_002_16"
patchType: "page"
start: "06:12"
end: "06:24"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | Your Health | menu"

[[pid: patch_002_17]]
--+-{ anchor_002_19 }-+--
patchTag: "Centers for Disease Control and Prevention"
domain: "cdc.gov"
pid: "patch_002_17"
patchType: "page"
start: "06:25"
end: "06:45"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | Science | menu"

[[pid: patch_002_18]]
--+-{ anchor_002_20 }-+--
patchTag: "Centers for Disease Control and Prevention"
domain: "cdc.gov"
pid: "patch_002_18"
patchType: "page"
start: "06:46"
end: "06:57"
HCI1: "scroll | down | page"
HCI2: "click | close | pop-up"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "click | More | menu"

[[pid: patch_002_19]]
--+-{ anchor_002_21 }-+--
patchTag: "Centers for Disease Control and Prevention"
domain: "cdc.gov"
pid: "patch_002_19"
patchType: "page"
start: "06:58"
end: "07:05"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_002_4]]
--+-{ anchor_002_22 }-+--
patchTag: "U.S. Department of Defense"
domain: "media.defense.gov"
pid: "patch_002_4"
patchType: "file"
start: "07:06"
end: "07:08"

[[pid: patch_002_3]]
--+-{ anchor_002_23 }-+--
patchTag: "United States Agency for International Development"
domain: "usaid.gov"
pid: "patch_002_3"
patchType: "file"
start: "07:10"
end: "07:26"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_002_19]]
--+-{ anchor_002_24 }-+--
patchTag: "Centers for Disease Control and Prevention"
domain: "cdc.gov"
pid: "patch_002_19"
patchType: "page"
start: "07:27"
end: "07:30"

[[pid: patch_002_11]]
--+-{ anchor_002_25 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_002_11"
patchType: "SERP"
start: "07:31"
end: "07:56"
HCI1: "type | Origin of Covid: Lab or through animals? | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_002_20]]
--+-{ anchor_002_26 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_002_20"
patchType: "SERP"
start: "07:57"
end: "08:20"
HCI1: "click | Do three studies add up to proof of COVID-19's origin in a... | result 1 title"
HCI2: "scroll | down | page"
HCI3: "click | Lab Leak Theory or Animal Origin? A Discussion on What... | result 7 title"
HCI4: "scroll | down | page"

[[pid: patch_002_21]]
--+-{ anchor_002_27 }-+--
patchTag: "Science"
domain: "science.org"
pid: "patch_002_21"
patchType: "page"
start: "08:21"
end: "10:06"
HCI1: "scroll | down | page"
