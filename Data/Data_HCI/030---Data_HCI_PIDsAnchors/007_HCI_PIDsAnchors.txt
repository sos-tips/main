[[sourceId=source007]]
[[streamId=HCI]]
[[cid=case007]]

[[pid: patch_007_1]]
--+-{ anchor_007_1 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_007_1"
patchType: "engine"
start: "00:00"
end: "00:12"
HCI1: "type | covid 19 origins | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_007_2]]
--+-{ anchor_007_2 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_007_2"
patchType: "engine"
start: "00:13"
end: "00:17"
HCI1: "type | covid 19 lab leak | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_007_3]]
--+-{ anchor_007_3 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_007_3"
patchType: "engine"
start: "00:18"
end: "00:24"
HCI1: "type | covid 19 bat to human | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_007_4]]
--+-{ anchor_007_4 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_007_4"
patchType: "SERP"
start: "00:25"
end: "01:18"
HCI1: "scroll | down | page"
HCI2: "click | Coronaviruses in humans and animals: the role of bats in viral... | result 8 title"
HCI3: "scroll | down | page"
HCI4: "click | Here's how scientists know the coronavirus came from bats... | result 9 title"
HCI5: "scroll | down | page"
HCI6: "click | Animals and COVID-19 / CDC | result 16 title"
HCI7: "scroll | down | page"
HCI8: "click | Understanding the Human-Animal Interplay of COVID-19 and... | result 17 title"

[[pid: patch_007_5]]
--+-{ anchor_007_5 }-+--
patchTag: "School of Veterinary Medicine"
domain: "vetmed.wisc.edu"
pid: "patch_007_5"
patchType: "page"
start: "01:19"
end: "02:18"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_007_6]]
--+-{ anchor_007_6 }-+--
patchTag: "Centers for Disease Control and Prevention"
domain: "cdc.gov"
pid: "patch_007_6"
patchType: "page"
start: "02:19"
end: "02:36"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | About COVID-19 | menu"

[[pid: patch_007_7]]
--+-{ anchor_007_7 }-+--
patchTag: "Centers for Disease Control and Prevention"
domain: "cdc.gov"
pid: "patch_007_7"
patchType: "page"
start: "02:37"
end: "02:40"
HCI1: "click | Frequently Asked Questions | menu"

[[pid: patch_007_7]]
--+-{ anchor_007_8 }-+--
patchTag: "Centers for Disease Control and Prevention"
domain: "cdc.gov"
pid: "patch_007_7"
patchType: "page"
start: "02:41"
end: "03:32"
HCI1: "scroll | down | page"
HCI2: "click | What is COVID-19? | hyperlink "
HCI3: "scroll | down | page"
HCI4: "click | Can bats in the United States get the virus that causes COVID-19, and can they spread it back to people? | hyperlink "
HCI5: "scroll | down | page"

[[pid: patch_007_8]]
--+-{ anchor_007_9 }-+--
patchTag: "National Library of Medicine"
domain: "ncbi.nlm.nih.gov"
pid: "patch_007_8"
patchType: "page"
start: "03:33"
end: "04:15"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_007_4]]
--+-{ anchor_007_10 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_007_4"
patchType: "SERP"
start: "04:16"
end: "04:22"
HCI1: "scroll | up | page"
HCI2: "click | WHO, China Report Suggests COVID-19 Passed From Bats to... | result 1 title"

[[pid: patch_007_9]]
--+-{ anchor_007_11 }-+--
patchTag: "Contagion Live"
domain: "contagionlive.com"
pid: "patch_007_9"
patchType: "page"
start: "04:23"
end: "04:56"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_007_10]]
--+-{ anchor_007_12 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_007_10"
patchType: "SERP"
start: "04:57"
end: "05:39"
HCI1: "click | From 'open-minded' to 'underwhelming' mixed reactions greet... | result 1 title"
HCI2: "click | Do three new studies add up to proof of COVID-19's origin in a... | result 1 subtitle"
HCI3: "scroll | down | page"
HCI4: "click | Deconstructed: The Lab-Leak Theory Is Looking Stronger | result 7 title"
HCI5: "scroll | down | page"
HCI6: "click | Unclassified-Summary-of-Assessment-on-COVID-19-Origins.pdf | result 16 title"
HCI7: "scroll | down | page"
HCI8: "click | News: The Covid-19 Lab Leak Theory Is a Tale ... (WIRED) | result 18 title"

[[pid: patch_007_11]]
--+-{ anchor_007_13 }-+--
patchTag: "National Library of Medicine"
domain: "ncbi.nlm.nih.gov"
pid: "patch_007_11"
patchType: "page"
start: "05:40"
end: "05:56"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"
HCI6: "click | Read more | hyperlink"

[[pid: patch_007_12]]
--+-{ anchor_007_14 }-+--
patchTag: "WIRED"
domain: "wired.com"
pid: "patch_007_12"
patchType: "page"
start: "05:57"
end: "07:50"
HCI1: "scroll | down | page"
HCI2: "click | close | pop-up"
HCI3: "scroll | down | page"
HCI4: "hover | strongly suggests | hyperlink"
HCI5: "scroll | down | page"

[[pid: patch_007_13]]
--+-{ anchor_007_15 }-+--
patchTag: "Director of National Intelligence"
domain: "dni.gov"
pid: "patch_007_13"
patchType: "file"
start: "07:51"
end: "08:54"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_007_14]]
--+-{ anchor_007_16 }-+--
patchTag: "The Intercept"
domain: "theintercept.com"
pid: "patch_007_14"
patchType: "page"
start: "08:55"
end: "10:04"
HCI1: "click | close | pop-up"
HCI2: "scroll | down | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
