[[sourceId=source011]]
[[streamId=HCI]]
[[cid=case011]]

[[pid: patch_011_1]]
--+-{ anchor_011_1 }-+--
patchTag: "Zoom"
domain: "zoom.us"
pid: "patch_011_1"
patchType: "page"
start: "00:00"
end: "00:25"
HCI1: "type | origin of covid 19 | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_011_2]]
--+-{ anchor_011_2 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_011_2"
patchType: "SERP"
start: "00:26"
end: "01:08"
HCI1: "scroll | down | page"
HCI2: "hover | Investigate the origins of COVID-19 - Science | result 2 title"
HCI3: "scroll | up | page"
HCI4: "scroll | down | page"
HCI5: "scroll | up | page"
HCI6: "scroll | down | page"
HCI7: "highlight | Ad http://www.un.org What is COVID-19? - UN Coronavirus Information - un.org | ad"
HCI7: "click | What is COVID-19? - UN Coronavirus Information - un.org | ad"

[[pid: patch_011_3]]
--+-{ anchor_011_3 }-+--
patchTag: "United Nations"
domain: "un.org"
pid: "patch_011_3"
patchType: "page"
start: "01:11"
end: "01:43"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"
HCI6: "scroll | up | page"
HCI7: "scroll | down | page"
HCI8: "scroll | up | page"
HCI9: "scroll | down | page"

[[pid: patch_011_2]]
--+-{ anchor_011_4 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_011_2"
patchType: "SERP"
start: "01:44"
end: "01:56"
HCI1: "scroll | down | page"
HCI2: "highlight | http://cnn.com > health > wuhan-market-covid-19 Covid-19 origins: New studies agree that animals sold.. - CNN | result 1 title"
HCI3: "click | Covid-19 origins: New studies agree that animals sold.. - CNN | result 1 title"

[[pid: patch_011_4]]
--+-{ anchor_011_5 }-+--
patchTag: "CNN"
domain: "cnn.com"
pid: "patch_011_4"
patchType: "page"
start: "01:57"
end: "03:25"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "click | posted online as preprints | hyperlink"
HCI5: "scroll | down | page"
HCI6: "scroll | up | page"
HCI7: "scroll | down | page"
HCI8: "click | close | pop-up"

[[pid: patch_011_5]]
--+-{ anchor_011_6 }-+--
patchTag: "CNN"
domain: "cnn.com"
pid: "patch_011_5"
patchType: "page"
start: "03:26"
end: "03:40"
HCI1: "scroll | down | page"
HCI2: "hover | One of the studies | hyperlink"
HCI3: "click | other study | hyperlink"

[[pid: patch_011_6]]
--+-{ anchor_011_7 }-+--
patchTag: "Zenodo"
domain: "zenodo.org"
pid: "patch_011_6"
patchType: "page"
start: "03:41"
end: "03:51"

[[pid: patch_011_5]]
--+-{ anchor_011_8 }-+--
patchTag: "CNN"
domain: "cnn.com"
pid: "patch_011_5"
patchType: "page"
start: "03:52"
end: "04:01"
HCI1: "scroll | down | page"

[[pid: patch_011_6]]
--+-{ anchor_011_9 }-+--
patchTag: "Zenodo"
domain: "zenodo.org"
pid: "patch_011_6"
patchType: "page"
start: "04:02"
end: "06:36"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"
HCI6: "scroll | up | page"
HCI7: "scroll | down | page"
HCI8: "scroll | up | page"
HCI9: "scroll | down | page"
HCI10: "scroll | up | page"
HCI11: "scroll | down | page"
HCI12: "scroll | up | page"

[[pid: patch_011_7]]
--+-{ anchor_011_10 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_011_7"
patchType: "engine"
start: "06:38"
end: "06:44"

[[pid: patch_011_2]]
--+-{ anchor_011_11 }-+--
patchTag: "Google All"
domain: "google.com"
pid: "patch_011_2"
patchType: "SERP"
start: "06:45"
end: "07:05"
HCI1: "scroll | down | page"
HCI2: "click | Will the World Ever Solve the Mystery of COVID-19's Origin? | result 8 title"

[[pid: patch_011_8]]
--+-{ anchor_011_12 }-+--
patchTag: "Council on Foreign Relations"
domain: "cfr.org"
pid: "patch_011_8"
patchType: "page"
start: "07:06"
end: "10:04"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "hover | a conspiracy theory | hyperlink"
HCI5: "scroll | down | page"
HCI6: "scroll | up | page"
HCI7: "scroll | down | page"
HCI8: "scroll | up | page"
HCI9: "scroll | down | page"
HCI10: "click | highly unlikely | hyperlink"

