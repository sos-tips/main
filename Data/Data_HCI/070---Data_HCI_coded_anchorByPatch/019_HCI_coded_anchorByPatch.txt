[[sourceId=source019]]
[[streamId=HCI]]
[[cid=case019]]

[[pid: patch_019_1]]
--+-{ anchor_019_1 }-+--
HCI1: "type | Origin of coronavirus | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_019_2]]
--+-{ anchor_019_2 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | Wuhan market was epicentre of pandemic's start, studies... | result 5 title"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"
HCI6: "click | Origins of Coronaviruses / NIH | result 1 title"
HCI7: "scroll | down | page"
HCI8: "scroll | up | page"
HCI9: "scroll | down | page"

[[pid: patch_019_3]]
--+-{ anchor_019_3 }-+--
HCI1: "scroll | down | page"

[[pid: patch_019_4]]
--+-{ anchor_019_4 }-+--
HCI1: "highlight | civets | page"

[[pid: patch_019_1]]
--+-{ anchor_019_5 }-+--
HCI1: "type | civets | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_019_5]]
--+-{ anchor_019_6 }-+--

[[pid: patch_019_4]]
--+-{ anchor_019_7 }-+--
HCI1: "scroll | down | page"

[[pid: patch_019_2]]
--+-{ anchor_019_8 }-+--
HCI1: "scroll | up | page"
HCI2: "type | origin of covid-19 | search bar"
HCI3: "type | enter | search bar"

[[pid: patch_019_6]]
--+-{ anchor_019_9 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Origins of SARS-CoV-2 / John Hopkins | result 1 title"
HCI3: "scroll | down | page"

[[pid: patch_019_7]]
--+-{ anchor_019_10 }-+--
HCI1: "scroll | down | page"
HCI2: "highlight | knell | page"

[[pid: patch_019_1]]
--+-{ anchor_019_11 }-+--
HCI1: "type | knell | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_019_8]]
--+-{ anchor_019_12 }-+--

[[pid: patch_019_7]]
--+-{ anchor_019_13 }-+--
HCI1: "scroll | down | page"
HCI2: "highlight | geospatial | page"
HCI3: "scroll | down | page"
HCI4: "highlight | spillover | page"
HCI5: "scroll | down | page"
HCI6: "highlight | Raccoon dogs | page"

[[pid: patch_019_1]]
--+-{ anchor_019_14 }-+--
HCI1: "type | Racoon dogs | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_019_9]]
--+-{ anchor_019_15 }-+--

[[pid: patch_019_7]]
--+-{ anchor_019_16 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "click | The Contested Origin of SARS-CoV-2 | hyperlink" [[APPR>HCI_Refs]]

[[pid: patch_019_10]]
--+-{ anchor_019_17 }-+--
HCI1: "click | View PDF | hyperlink"

[[pid: patch_019_11]]
--+-{ anchor_019_18 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_019_10]]
--+-{ anchor_019_19 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "highlight | dispassionate science-based discourse | page"
HCI5: "scroll | down | page"
HCI6: "hover | 7 | hyperlink" [[APPR>HCI_Refs]]