[[sourceId=source017]]
[[streamId=HCI]]
[[cid=case017]]

[[pid: patch_017_1]]
--+-{ anchor_017_1 }-+--
HCI1: "type | covid-19 origin | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_017_2]]
--+-{ anchor_017_2 }-+--
HCI1: "click | Investigations into the origin of COVID-19 - Wikipedia | result 1 title"
HCI2: "click | The Origin of COVID-19 and Why It Matter -PubMed | result 2 title"

[[pid: patch_017_3]]
--+-{ anchor_017_3 }-+--
HCI1: "scroll | down | page"

[[pid: patch_017_4]]
--+-{ anchor_017_4 }-+--
HCI1: "scroll | down | page"
HCI2: "hover | Am J Trop Med Hyg | hyperlink"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "click | Full text | hyperlink"

[[pid: patch_017_5]]
--+-{ anchor_017_5 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"

[[pid: patch_017_4]]
--+-{ anchor_017_6 }-+--
HCI1: "type | https://scholar.google.fr | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_017_6]]
--+-{ anchor_017_7 }-+--
HCI1: "type | covid-19 emergence | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_017_7]]
--+-{ anchor_017_8 }-+--
HCI1: "hover | COVID-19: emergence, spread, possible treatments, and global burden | result 2 title"
HCI2: "hover | COVID-19 infection: Emergence, transmission, and characteristics of human coronaviruses | result 1 title"

[[pid: patch_017_4]]
--+-{ anchor_017_9 }-+--

[[pid: patch_017_7]]
--+-{ anchor_017_10 }-+--
HCI1: "scroll | down | page"
HCI2: "type | covid-19 origin | search bar"
HCI3: "type | enter | search bar"

[[pid: patch_017_8]]
--+-{ anchor_017_11 }-+--
HCI1: "hover | A review on COVID-19: origin, spread, symptoms, treatment, and prevention | result 2 title"
HCI2: "hover | The origin, transmission and clinical therapies on coronavirus disease 2019 (COVID-19) outbreak - an update on the status | result 3 title"
HCI3: "scroll | down | page"
HCI4: "click | The origin, transmission and clinical therapies on coronavirus disease 2019 (COVID-19) outbreak - an update on the status | result 3 title"

[[pid: patch_017_9]]
--+-{ anchor_017_12 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"
HCI6: "scroll | up | page"
HCI7: "scroll | down | page"