[[sourceId=source012]]
[[streamId=HCI]]
[[cid=case012]]

[[pid: patch_012_1]]
--+-{ anchor_012_1 }-+--
HCI1: "type | covid 19 origins | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_012_2]]
--+-{ anchor_012_2 }-+--
HCI1: "scroll | down | page"
HCI2: "hover | Covid-19 origins: New studies agree that animals sold... - CNN | result 1 title"
HCI3: "scroll | down | page"
HCI4: "hover | From 'open-minded' to 'underwhelming' mixed reactions greet... | result 2 title"
HCI5: "scroll | down | page"
HCI6: "click | Origins of Coronaviruses / NIH | result 4 title"

[[pid: patch_012_3]]
--+-{ anchor_012_3 }-+--
HCI1: "scroll | down | page"
HCI2: "click | SARS-COV-2 and NIAID Supported Bat Coronavirus Research | hyperlink" [[APPR>HCI_Refs]]

[[pid: patch_012_4]]
--+-{ anchor_012_4 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_012_2]]
--+-{ anchor_012_5 }-+--
HCI1: "scroll | down | page"
HCI2: "hover | WHO Report: Origin of COVID-19 Remains to be Determined | result 6 title"
HCI3: "scroll | down | page"
HCI4: "hover | Searching for SARS-CoV-2 origins: the saga continues | result 7 title"
HCI5: "click | Covid origin studies say evidence points to Wuhan market - BBC | result 8 title"

[[pid: patch_012_5]]
--+-{ anchor_012_6 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "highlight | 2 day | page" [[APPR>HCI_Date]]
HCI4: "scroll | down | page"
HCI5: "click | were clustered around that market | hyperlink" [[APPR>HCI_Refs]]

[[pid: patch_012_6]]
--+-{ anchor_012_7 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"

[[pid: patch_012_2]]
--+-{ anchor_012_8 }-+--
HCI1: "scroll | up | page"
HCI2: "type | covid 19 | search bar"
HCI3: "type | enter | search bar"

[[pid: patch_012_7]]
--+-{ anchor_012_9 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Coronavirus disease (COVID-19) pandemic | result 2 title"

[[pid: patch_012_8]]
--+-{ anchor_012_10 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Origins of the SARS-CoV-2 virus | menu"

[[pid: patch_012_9]]
--+-{ anchor_012_11 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "hover | WHO-convened global study of the origins of SARS-CoV-2: China Part | hyperlink"
HCI4: "click | WHO calls for further studies, data on origin of SARS-CoV-2 virus, reiterates that all hyp... | hyperlink"

[[pid: patch_012_10]]
--+-{ anchor_012_12 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "highlight | 0 March 20 | page" [[APPR>HCI_Date]]
HCI5: "scroll | down | page"

[[pid: patch_012_5]]
--+-{ anchor_012_13 }-+--
HCI1: "scroll | up | page"

[[pid: patch_012_6]]
--+-{ anchor_012_14 }-+--
HCI1: "scroll | up | page"
HCI2: "scroll | down | page"

[[pid: patch_012_10]]
--+-{ anchor_012_15 }-+--

[[pid: patch_012_7]]
--+-{ anchor_012_16 }-+--
HCI1: "scroll | down | page"
HCI2: "click | COVID-19 - Wikipedia | result 5 title"

[[pid: patch_012_11]]
--+-{ anchor_012_17 }-+--
HCI1: "scroll | down | page"
HCI2: "click | History | hyperlink"