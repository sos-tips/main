[[sourceId=source003]]
[[streamId=HCI]]
[[cid=case003]]

[[pid: patch_003_1]]
--+-{ anchor_003_1 }-+--
HCI1: "type | covid-19 origins | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_003_2]]
--+-{ anchor_003_2 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Coronavirus (COVID-19) origin: Cause and how it spreads | result 1 title"

[[pid: patch_003_3]]
--+-{ anchor_003_3 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Accept cookies | pop-up"
HCI3: "click | World Health Organization (WHO) | hyperlink" [[APPR>HCI_Refs]]
HCI4: "scroll | down | page"
HCI5: "highlight | However, it remains unclear exactly how the virus first spread to humans. | page"
HCI6: "scroll | down | page"
# The next website was loading for a long time, he checked it twice, but as it was still loading, he clicked back to read this page while the next one was loading (try to move to next patch between 1:22-1:37 and 1:55-1:56).

[[pid: patch_003_4]]
--+-{ anchor_003_4 }-+--
HCI1: "scroll | down | page"

[[pid: patch_003_5]]
--+-{ anchor_003_5 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Wuhan market was epicentre of pandemic's start, studies... | result 3 title"

[[pid: patch_003_6]]
--+-{ anchor_003_6 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Accept cookies | pop-up"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"
HCI6: "highlight | Scientists have released three studies that reveal intriguing new clues about how the COVID-19 pandemic started. Two of the reports trace the outbreak back to a massive market that sold live animals, among other goods, in Wuhan, China1,2, and a third suggests that the coronavirus SARS-CoV-2 spilled over from animals — possibly those sold at the market — to humans at least twice in November or December 2019. Posted on 25 and 26 February, all three are preprints, and so have not been published in a peer-reviewed journal. | page"
HCI7: "scroll | up | page"
HCI8: "scroll | down | page"
HCI9: "scroll | up | page"
HCI10: "highlight | These analyses add weight to original suspicions that the pandemic began at the Huanan Seafood Wholesale Market, which many of the people who were infected earliest with SARS-CoV-2 had visited. The preprints contain genetic analyses of coronavirus samples collected from the market and from people infected in December 2019 and January 2020, as well as geolocation analyses connecting many of the samples to a section of the market where live animals were sold. Taken together, these l | page"

[[pid: patch_003_5]]
--+-{ anchor_003_7 }-+--
HCI1: "scroll | down | page"
HCI2: "click | COVID-19: The endless search for the origins of the virus - Al... | result 4 title"

[[pid: patch_003_7]]
--+-{ anchor_003_8 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "click | close | pop-up"