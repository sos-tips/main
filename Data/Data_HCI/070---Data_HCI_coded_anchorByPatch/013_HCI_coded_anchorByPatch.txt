[[sourceId=source013]]
[[streamId=HCI]]
[[cid=case013]]

[[pid: patch_013_1]]
--+-{ anchor_013_1 }-+--
HCI1: "type | origin of covid 19 | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_013_2]]
--+-{ anchor_013_2 }-+--
HCI1: "scroll | down | page"
HCI2: "hover | Do three new studies add up to proof of COVID-19's origin in a... | result 1 subtitle"
HCI1: "scroll | down | page"
HCI1: "click | Origins of Coronaviruses / NIH | result 2 title"
HCI1: "scroll | down | page"
HCI2: "hover | COVID-19: An Insight into SARS-CoV-2 Pandemic Originates... | result 5 title"
HCI2: "hover | WHO Report: Origin of COVID-19 Remains to be Determined | result 6 title"
HCI2: "click | COVID-19: An Insight into SARS-CoV-2 Pandemic Originates... | result 5 title"

[[pid: patch_013_3]]
--+-{ anchor_013_3 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "highlight | ARS-CoV and MERS-CoV originated in bats. SARS-CoV then spread from infected civets to people, while MERS-CoV spreads fro | page"

[[pid: patch_013_1]]
--+-{ anchor_013_4 }-+--

[[pid: patch_013_3]]
--+-{ anchor_013_5 }-+--

[[pid: patch_013_1]]
--+-{ anchor_013_6 }-+--
HCI1: "type | what is the difference between SARS COV and mers cov | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_013_4]]
--+-{ anchor_013_7 }-+--
HCI1: "highlight | RS-CoV-2 shares only 40% sequence identity with MERS-CoV, which is a lineage C bet | result 1 snippet"

[[pid: patch_013_3]]
--+-{ anchor_013_8 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "highlight | tely, because the origins of the SARS-CoV-2 have not yet been identified, misleading and false allegations have been made about NI | page"
HCI5: "scroll | down | page"
HCI6: "highlight | Wuhan Institute of Virology in Wuhan, China | page"
HCI7: "type | CTRL + C | page"

[[pid: patch_013_1]]
--+-{ anchor_013_9 }-+--
HCI1: "type | CRTL + V | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_013_5]]
--+-{ anchor_013_10 }-+--
HCI1: "click | Wuhan Institute of Virology | result 1 title"

[[pid: patch_013_6]]
--+-{ anchor_013_11 }-+--

[[pid: patch_013_5]]
--+-{ anchor_013_12 }-+--
HCI1: "click | Fact Sheet at the Wuhan Institute of Virology Wuhan Institute of Virology - state.gov | result 3 title"

[[pid: patch_013_7]]
--+-{ anchor_013_13 }-+--
HCI1: "scroll | down | page"
HCI2: "highlight | has systematically prevented a transparent and thorough investigation of the COVID-19 pandemic’s | page"
HCI3: "highlight | t and disinformation. Nearly two million people have died. Their families deserve to know the truth. Only through transparency can we learn what caused this pandemic and how to prevent the next one. The U.S. governme | page" [[APPR>HCI_AuthAffil]]
HCI4: "scroll | down | page"
HCI5: "highlight | here, when, or how the COVID-19 virus—known as SARS-CoV-2—was transmitted initially to humans. We have not determined whether the outbreak began through contact with infected animals or wa | page"
HCI6: "highlight | rus could have emerged naturally from human contact with infected animals, spreading in a patte | page"
HCI7: "highlight | th a natural epidemic. Alternatively, a laboratory accident could resemble a natural outbreak if the initial | page"
HCI8: "scroll | down | page"
HCI9: "highlight | esses inside the Wuhan Institute of Virology (WIV): T | page"
HCI10: "highlight | e the first identified case of the outbreak, with symptoms consistent with both COVID-19 and common seasonal illnesses. This raises questions about the credibility of WIV senior researcher Shi Zhengli's public claim that there was 'zero infection' among the WIV’s staff and students of SARS-Co | page"
HCI11: "highlight | ental infections in labs have caused several previous virus outbreaks in China and elsewhere, including a 2004 SARS outbreak in Beijing that infected nine people, killing one. The | page"

[[pid: patch_013_1]]
--+-{ anchor_013_14 }-+--
HCI1: "type | what's the difference between sars and covid 19 | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_013_8]]
--+-{ anchor_013_15 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "click | COVID-19 and SARS: Differences and similarities - PMC - NCBI | result 3 title"

[[pid: patch_013_9]]
--+-{ anchor_013_16 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"

[[pid: patch_013_3]]
--+-{ anchor_013_17 }-+--

[[pid: patch_013_1]]
--+-{ anchor_013_18 }-+--
HCI1: "type | how do bats get covid | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_013_10]]
--+-{ anchor_013_19 }-+--
HCI1: "click | COVID-19 and Bats - Bats and diseases - Bat Conservation Trust | result 1 title"

[[pid: patch_013_11]]
--+-{ anchor_013_20 }-+--
HCI1: "highlight | page will be reviewed regularly and revised as necessary in light of new information. Research papers about Coronavirus Disease (COVID-19) and the virus that causes it (SARS-CoV-2) are being produced | page"
HCI2: "scroll | down | page"
HCI3: "highlight | e virus that causes COVID-19 hasn’t been isolated from any of the UK’s 17 resident breeding bat species. There are no known zoonotic (harmful to humans) coronaviruses found in UK bats. In fact the CO | page"
HCI4: "highlight | n fact the COVID-19 virus hasn’t been isolated from any of the world’s 1400+ species of bat. A coronavirus with 96% of its genome in common with SARS-CoV-2 (2) has been found in a single species of bat (Rhinolophus affinis) in China. This may sound significant, but to put it in context, we share | page"
HCI5: "highlight | nzees but we are not the same species. It is important to stress that in this pandemic it is hum | page"
HCI6: "scroll | down | page"
HCI7: "highlight | Zoonotic spillover is the transmission of a pathogen from an animal to a human, often through an intermediary species. For such an event to happen, zoonotic pathogens must overcome a series of barriers t | page"
HCI8: "highlight | (5). If we fail to care for the natural world and all of the services that it provides to humanity then ecosystems can break down in ways that have significant impacts on us, including through emerging infectious diseases (6). | page"
HCI9: "scroll | down | page"
HCI10: "hover | Pathways to zoonotic spillover | hyperlink" [[APPR>HCI_Refs]]
HCI11: "hover | The Ecology of Disease | hyperlink" [[APPR>HCI_Refs]]
HCI12: "scroll | up | page"
HCI13: "scroll | down | page"
HCI14: "highlight | No, the pandemic was caused by people. Human activities that alter the environment can increase the | page"
HCI15: "highlight | He argues that we specifically made this pandemic (or epidemic as it was then) by engaging in unsustainable ecological destruction and the dangerous and devastating trafficking and illegal trade of wildlife for human consumption. | page"
HCI16: "scroll | up | page"
HCI17: "highlight | reaks, in fact this may well increase the dangers since stressed animals may become more disease prone. It is human activity that led to the current pandemic and it will be changing human behaviour in rela | page"
HCI18: "scroll | down | page"
HCI19: "highlight | nt future outbreaks we need to stop uncontrolled habitat destruction and control the trade in wild animals. See ‘How did COVID-19 get from wildlife to people?’ above. | page"
HCI20: "scroll | up | page"
HCI21: "highlight | e live wildlife trade, in which many different species of wild animals are brought together in markets, can also provide the conditions for spillover events. The traded animals are held in cramped, stressful, unsanitary conditions, with many different species caged side by side and slaughtered to order. The species in these markets would not be found together in such close proximity in the wild. Such trade increases the chances that viruses can jump from one species to another, and ultimately from animals to humans. The science writer, David Quammen, who has published two books on wildlife and diseases, wrote about this back in January (9). He argues that we specifically made this pandemic (or epidemic as it was then) by engaging in unsustainable ecological destruction and the dangerous and devastating t | page"

[[pid: patch_013_3]]
--+-{ anchor_013_21 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | SARS-COV-2 and NIAID Supported Bat Coronavirus Research | hyperlink" [[APPR>HCI_Refs]]

[[pid: patch_013_12]]
--+-{ anchor_013_22 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "highlight | sin converting enzyme-2 (ACE2) protein to help enter and infect host cells. In order to study animal coronaviruses circulating in nature, the investigators replaced the spike prote | page"
HCI4: "highlight | om a well-characterized bat coronavirus, WIV1-CoV, with the spike protein of animal coronaviruses recently discovered in bats in China. Using techniques common in virology, experiments involved a single round of infection in several cell lines, and in some cases, in mice that were genetically modified to express the human version of ACE2. All other aspects of the mice, including the immune system, remained unchanged. The ACE2 transgenic mice were used to determine if spike proteins from bat coronaviruses discovered in | page"
HCI5: "highlight | China were capable of binding human ACE2, and therefore, whether the bat coronaviruses themselves, which were already present in th | page"
HCI6: "scroll | down | page"
HCI7: "scroll | up | page"
HCI8: "highlight | The ACE2 transgenic mice were used to determine if spike proteins from bat coronaviruses discovered in China were capable of binding human ACE2, and therefore, whether the bat coronaviruses themselves, which were already present in the environment, could potentially infect humans and cause disease. WIV1-CoV is not known to cause infection in humans but has been shown in the laboratory to infect both human cells and ACE2 transgenic mice (ref), making it an ideal tool to use for these studies. Several of the bat coronaviruses used in these experiments | page"
HCI9: "highlight | everal of the bat coronaviruses used in these experiments were also found to be capable of replicating in ACE2 transgenic mice, indicating that the spike protein from the naturally occurring bat coronaviruses from which they were made could bind ACE2 in vi | page"
HCI10: "scroll | down | page"
HCI11: "highlight | NIH-funded research had a role in the emergence of SARS-CoV-2. In this regard, the chimeric viruses that were studied (i.e., the WIV-1 virus with the various spike proteins obtained from bat viruses foun | page" [[APPR>HCI_AuthAffil]]
HCI12: "highlight | from an evolutionary standpoint from SARS-CoV-2 (Figure 1) that they could not have possibly been the source of SARS-CoV-2 or the | page"
HCI13: "scroll | down | page"
HCI14: "highlight | ite the similarity of RaTG13 and BANAL-52 bat coronaviruses (orange bars) to SARS-CoV-2 (red bars), experts agree that even these viruses are far too divergent to have been the progenitor of SARS-CoV-2, further highlighting that the bat coronaviruses stu | page"
HCI15: "scroll | down | page"
HCI16: "highlight | rgent viruses that failed to replicate in cells are not shown (ref). Credit: NIAID | page" [[APPR>HCI_Refs]] [[APPR>HCI_AuthAffil]]
HCI17: "scroll | up | page"
HCI18: "highlight | periments under the NIH grant to EcoHealth Alliance and reported in the scientific literature (ref) or annual progress reports. From this analysis, it is evident that the viruses studied under the EcoHealth Alliance grant are very far distant from SARS-CoV-2. Included for comparison is RaTG13, one of the closest bat coronavirus relatives to SARS-CoV-2 collected by the Wuhan Institute of Virology (ref) and BANAL-52, one of several bat coronaviruses recently identified from bats living in caves in Laos (ref). Although RaTG13 and BANAL-52 are 96-97% identical to SARS-CoV-2 at the nucleotide level (>900 nucleotide differences across the entire genome), the difference actually | page" [[APPR>HCI_Refs]]