[[sourceId=source010]]
[[streamId=HCI]]
[[cid=case010]]

[[pid: patch_010_1]]
--+-{ anchor_010_1 }-+--
HCI1: "type | covid origin | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_010_2]]
--+-{ anchor_010_2 }-+--
HCI1: "scroll | down | page
HCI2: "click | Scientists 'strongly condemn' rumors and conspiracy theories... | result 1 title"
HCI3: "scroll |down | page"
HCI4: "click | Compelling new evidence tracks COVID's origin to Wuhan... | result 2 title"
HCI5: "click | Origins of Coronaviruses / NIH' | result 3 title"
HCI6: "scroll | down | page"
HCI7: "click | Covid-19 origins: New studies agree that animal sold... - CNN | result 4 title"
HCI8: "scroll | up | page"
HCI9: "type | covid lab leak | search bar"
HCI10: "type | enter | search bar"

[[pid: patch_010_3]]
--+-{ anchor_010_3 }-+--
HCI1: "scroll | down | page"
HCI2: "click |From 'open minded' to 'underwhelming,' mixed reactions greet... | result 2 title"
HCI3: "scroll | down | page"
HCI4: "click | COVID-19 lab leak theory - Wikipedia | result 5 title"

[[pid: patch_010_4]]
--+-{ anchor_010_4 }-+--
HCI1: "scroll | down | page"
HCI2: "click | SARS-related coronaviruses | hyperlink"
HCI3: "scroll | down | page"
HCI4: "hover | natural reservoir | hyperlink"
HCI5: "scroll | down | page"
HCI6: "hover | betacoronaviruses | hyperlink"
HCI7: "scroll | down | page"
HCI8: "click | photo| page"

[[pid: patch_010_5]]
--+-{ anchor_010_5 }-+--

[[pid: patch_010_4]]
--+-{ anchor_010_6 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Laboratory leak incidents | hyperlink

[[pid: patch_010_6]]
--+-{ anchor_010_7 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_010_7]]
--+-{ anchor_010_8 }-+--

[[pid: patch_010_4]]
--+-{ anchor_010_9 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Gain-of-function research & COVID-19 pandemic | hyperlink"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"

[[pid: patch_010_8]]
--+-{ anchor_010_10 }-+--

[[pid: patch_010_9]]
--+-{ anchor_010_11 }-+--

[[pid: patch_010_10]]
--+-{ anchor_010_12 }-+--
HCI1: "scroll | down | page"
HCI2: "click | published online | hyperlink"
HCI3: "scroll | down | page"

[[pid: patch_010_11]]
--+-{ anchor_010_13 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | SARS-COV-2 and NIAID-Supported Bat Coronavirus Research | hyperlink"

[[pid: patch_010_12]]
--+-{ anchor_010_14 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "highlight | Wertheim | page"

[[pid: patch_010_13]]
--+-{ anchor_010_15 }-+--
HCI1: "scroll | down | page"
