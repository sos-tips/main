[[sourceId=source008]]
[[streamId=HCI]]
[[cid=case008]]

[[pid: patch_008_1]]
--+-{ anchor_008_1 }-+--
HCI1: "type | origins of covid-19 | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_008_2]]
--+-{ anchor_008_2 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Mysteries Linger About Covid's Origins, W.H.O. Report Says | result 7 title"

[[pid: patch_008_3]]
--+-{ anchor_008_3 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"
HCI6: "scroll | up | page"

[[pid: patch_008_1]]
--+-{ anchor_008_4 }-+--
HCI1: "type | staff.library.wisc.edu | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_008_4]]
--+-{ anchor_008_5 }-+--
HCI1: "click | Library Website | menu"

[[pid: patch_008_5]]
--+-{ anchor_008_6 }-+--
HCI1: "click | Search the | filter"
HCI2: "click | Articles | filter"
HCI3: "type | COVID-19 origin | search bar"

[[pid: patch_008_3]]
--+-{ anchor_008_7 }-+--

[[pid: patch_008_2]]
--+-{ anchor_008_8 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | 'Updated Assessment on COVID-19 Origins - Key Takeaways' | result 3 title"

[[pid: patch_008_6]]
--+-{ anchor_008_9 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"
HCI6: "scroll | up | page"
HCI7: "type | dni.gov | search bar"
HCI8: "type | enter | search bar"

[[pid: patch_008_7]]
--+-{ anchor_008_10 }-+--
HCI1: "scroll | down | page"

[[pid: patch_008_2]]
--+-{ anchor_008_11 }-+--
HCI1: "scroll | down | page"
HCI2: "click | next page | pagination"

[[pid: patch_008_8]]
--+-{ anchor_008_12 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Covid-19 origin: Scientists tracked the outbreak to a... - Vox | result 12 title"

[[pid: patch_008_9]]
--+-{ anchor_008_13 }-+--
HCI1: "scroll | down | page"

[[pid: patch_008_10]]
--+-{ anchor_008_14 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | down | page"
HCI3: "click | Results sorted by | filter"
HCI4: "hover | Date | filter" 
HCI5: "click | Created Years | filter"
HCI6: "type | 2021 | filter"
HCI7: "click | Submit Que | filter"

[[pid: patch_008_11]]
--+-{ anchor_008_15 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | Resource Types | filter"
HCI4: "scroll | down | page"
HCI5: "scroll | up | page"
HCI6: "scroll | down | page"
HCI7: "scroll | up | page"
HCI8: "scroll | down | page"
HCI9: "scroll | up | page"
HCI10: "click | Investigate the origins of COVID-19 | result 12 title"

[[pid: patch_008_12]]
--+-{ anchor_008_16 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | Download PDF | hyperlink"

[[pid: patch_008_13]]
--+-{ anchor_008_17 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"

[[pid: patch_008_12]]
--+-{ anchor_008_18 }-+--

[[pid: patch_008_11]]
--+-{ anchor_008_19 }-+--
HCI1: "scroll | down | page"