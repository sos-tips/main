[[sourceId=source016]]
[[streamId=HCI]]
[[cid=case016]]

[[pid: patch_016_1]]
--+-{ anchor_016_1 }-+--
HCI1: "type | google | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_016_2]]
--+-{ anchor_016_2 }-+--
HCI1: "type | origin of covid-19 virus | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_016_3]]
--+-{ anchor_016_3 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "type | origin of covid-19 virus bbc | search bar"
HCI4: "type | enter | search bar"

[[pid: patch_016_4]]
--+-{ anchor_016_4 }-+--
HCI1: "click | Covid origin studies say evidence points to Wuhan market - BBC | result 1 title"

[[pid: patch_016_5]]
--+-{ anchor_016_5 }-+--
HCI1: "scroll | down | page"

[[pid: patch_016_4]]
--+-{ anchor_016_6 }-+--
HCI1: "click | Covid origin studies say evidence points to Wuhan market - BBC | result 1 title"

[[pid: patch_016_5]]
--+-{ anchor_016_7 }-+--
HCI1: "scroll | down | page"
HCI2: "click | were clustered around that market | hyperlink"

[[pid: patch_016_6]]
--+-{ anchor_016_8 }-+--
HCI1: "scroll | down | page"

[[pid: patch_016_5]]
--+-{ anchor_016_9 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "click | Scientists weight up evidence over COVID's origins | hyperlink"

[[pid: patch_016_7]]
--+-{ anchor_016_10 }-+--
HCI1: "scroll | down | page"

[[pid: patch_016_4]]
--+-{ anchor_016_11 }-+--
HCI1: "scroll | down | page"
HCI2: "click | What is the origin of COVID-19? | Common questions"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "type | origin of covid-19 virus guardian | search bar"
HCI6: "type | enter | search bar"

[[pid: patch_016_8]]
--+-{ anchor_016_12 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Angela Rasmussen on Covid-19: This origins discussion is... | result 1 subtitle"

[[pid: patch_016_9]]
--+-{ anchor_016_13 }-+--
HCI1: "click | close | pop-up"
HCI2: "scroll | down | page"
HCI3: "scroll | up | page"
HCI4: "scroll | down | page"

[[pid: patch_016_8]]
--+-{ anchor_016_14 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "type | origin of covid-19 virus uk government | search bar"
HCI4: "type | enter | search bar"

[[pid: patch_016_10]]
--+-{ anchor_016_15 }-+--
HCI1: "hover | COVID-19: background information - GOV.UK | result 1 title"
HCI2: "hover | COVID-19: epidemiology, virology and clinical features | result 1 subtitle"
HCI3: "click | COVID-19: background information - GOV.UK | result 1 title"

[[pid: patch_016_11]]
--+-{ anchor_016_16 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | COVID-19: epidemilogy, virology and clinical features | hyperlink"

[[pid: patch_016_12]]
--+-{ anchor_016_17 }-+--
HCI1: "scroll | down | page"

[[pid: patch_016_11]]
--+-{ anchor_016_18 }-+--

[[pid: patch_016_10]]
--+-{ anchor_016_19 }-+--
HCI1: "type | origin of covid-19 virus contraversy | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_016_13]]
--+-{ anchor_016_20 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Why it's so tricky to trace the origin of COVID-19 | result 5 title"

[[pid: patch_016_14]]
--+-{ anchor_016_21 }-+--
HCI1: "scroll | down | page"
HCI2: "click | one-page unclassified summary | hyperlink"

[[pid: patch_016_15]]
--+-{ anchor_016_22 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_016_14]]
--+-{ anchor_016_23 }-+--
HCI1: "scroll | down | page"
HCI2: "click | continue | pop-up"

[[pid: patch_016_13]]
--+-{ anchor_016_24 }-+--
HCI1: "scroll | down | page"
HCI2: "click | 'Tit for that': why hunt for Cocid's origins still mired in politics and... | result 8 title"

[[pid: patch_016_16]]
--+-{ anchor_016_25 }-+--
HCI1: "click | close | pop-up"
HCI2: "scroll | down | page"

