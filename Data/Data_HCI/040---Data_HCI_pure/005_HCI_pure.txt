[[sourceId=source005]]
[[streamId=HCI]]
[[cid=case005]]

[[pid: patch_005_1]]
--+-{ anchor_005_1 }-+--
HCI1: "type | pubmed | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_005_2]]
--+-{ anchor_005_2 }-+--
HCI1: "click | PubMed | result 1 title"

[[pid: patch_005_3]]
--+-{ anchor_005_3 }-+--
HCI1: "type | the origin of sars-cov-2 | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_005_4]]
--+-{ anchor_005_4 }-+--
HCI1: "scroll | down | page"
HCI2: "click | More filters | filter"

[[pid: patch_005_5]]
--+-{ anchor_005_5 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Filter | filter"
HCI3: "click | Etiology | filter"

[[pid: patch_005_6]]
--+-{ anchor_005_6 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Filter | filter"

[[pid: patch_005_1]]
--+-{ anchor_005_7 }-+--
HCI1: "type | etiology definition | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_005_7]]
--+-{ anchor_005_8 }-+--

[[pid: patch_005_6]]
--+-{ anchor_005_9 }-+--
HCI1: "scroll | down | page"
HCI2: "click | See all results in PubMed (776) | hyperlink" 

[[pid: patch_005_8]]
--+-{ anchor_005_10 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "type | (sars-cov-2) AND (Etiology/Broad[filter]) | search bar"
HCI4: "type | enter | search bar"

[[pid: patch_005_9]]
--+-{ anchor_005_11 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Meta-Analysis | filter"

[[pid: patch_005_10]]
--+-{ anchor_005_12 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Randomized Controlled Trial | filter"

[[pid: patch_005_11]]
--+-{ anchor_005_13 }-+--
HCI1: "click | 2004-2020 | filter"

[[pid: patch_005_12]]
--+-{ anchor_005_14 }-+--
HCI1: "scroll | down | page"
HCI2: "click | COVID-19: Discovery, diagnostics and drug development | result 3 title"

[[pid: patch_005_13]]
--+-{ anchor_005_15 }-+--
HCI1: "scroll | down | page"

[[pid: patch_005_12]]
--+-{ anchor_005_16 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | 2004-2022 | filter"

[[pid: patch_005_14]]
--+-{ anchor_005_17 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "type | (sars-cov-2) AND (Etiology/Broad[filter]) AND (wuhan) | search bar"
HCI4: "type | enter | search bar"

[[pid: patch_005_15]]
--+-{ anchor_005_18 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Infection-enhancing anti-SARS-CoV-2 antibodies recognize both the original Wuhan/D614G strain and Delta variants. A potential risk for mass vaccination? | result 9 title"

[[pid: patch_005_16]]
--+-{ anchor_005_19 }-+--
HCI1: "highlight | Wuhan/D614G strain | page"
HCI2: "type | Ctrl + C | page"
HCI3: "highlight | wuhan | search bar"
HCI4: "type | Ctrl + V | search bar"

[[pid: patch_005_15]]
--+-{ anchor_005_20 }-+--
HCI1: "scroll | up | page"
HCI2: "highlight | wuhan | search bar"
HCI3: "type | Ctrl + V | search bar"
HCI4: "type | enter | search bar"

[[pid: patch_005_17]]
--+-{ anchor_005_21 }-+--
HCI1: "type | (sars-cov-2) AND (Etiology/Broad[filter]) AND (614G strain) | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_005_18]]
--+-{ anchor_005_22 }-+--
HCI1: "scroll | down | page"

[[pid: patch_005_1]]
--+-{ anchor_005_23 }-+--
HCI1: "type | world health organization | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_005_19]]
--+-{ anchor_005_24 }-+--
HCI1: "click | Coronavirus disease (COVID-19) | result 1 subtitle"

[[pid: patch_005_20]]
--+-{ anchor_005_25 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Origins of the SARS-CoV-2 virus | menu"

[[pid: patch_005_21]]
--+-{ anchor_005_26 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Read More | hyperlink"

[[pid: patch_005_22]]
--+-{ anchor_005_27 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | Download (2.3 MB) | hyperlink"

[[pid: patch_005_23]]
--+-{ anchor_005_28 }-+--
HCI1: "scroll | down | page"
HCI2: "click | zoom in | page"
HCI3: "scroll | right | page"
HCI4: "scroll | down | page"

