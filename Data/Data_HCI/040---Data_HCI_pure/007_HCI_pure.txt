[[sourceId=source007]]
[[streamId=HCI]]
[[cid=case007]]

[[pid: patch_007_1]]
--+-{ anchor_007_1 }-+--
HCI1: "type | covid 19 origins | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_007_2]]
--+-{ anchor_007_2 }-+--
HCI1: "type | covid 19 lab leak | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_007_3]]
--+-{ anchor_007_3 }-+--
HCI1: "type | covid 19 bat to human | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_007_4]]
--+-{ anchor_007_4 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Coronaviruses in humans and animals: the role of bats in viral... | result 8 title"
HCI3: "scroll | down | page"
HCI4: "click | Here's how scientists know the coronavirus came from bats... | result 9 title"
HCI5: "scroll | down | page"
HCI6: "click | Animals and COVID-19 / CDC | result 16 title"
HCI7: "scroll | down | page"
HCI8: "click | Understanding the Human-Animal Interplay of COVID-19 and... | result 17 title"

[[pid: patch_007_5]]
--+-{ anchor_007_5 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_007_6]]
--+-{ anchor_007_6 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | About COVID-19 | menu"

[[pid: patch_007_7]]
--+-{ anchor_007_7 }-+--
HCI1: "click | Frequently Asked Questions | menu"

[[pid: patch_007_7]]
--+-{ anchor_007_8 }-+--
HCI1: "scroll | down | page"
HCI2: "click | What is COVID-19? | hyperlink "
HCI3: "scroll | down | page"
HCI4: "click | Can bats in the United States get the virus that causes COVID-19, and can they spread it back to people? | hyperlink "
HCI5: "scroll | down | page"

[[pid: patch_007_8]]
--+-{ anchor_007_9 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_007_4]]
--+-{ anchor_007_10 }-+--
HCI1: "scroll | up | page"
HCI2: "click | WHO, China Report Suggests COVID-19 Passed From Bats to... | result 1 title"

[[pid: patch_007_9]]
--+-{ anchor_007_11 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_007_10]]
--+-{ anchor_007_12 }-+--
HCI1: "click | From 'open-minded' to 'underwhelming' mixed reactions greet... | result 1 title"
HCI2: "click | Do three new studies add up to proof of COVID-19's origin in a... | result 1 subtitle"
HCI3: "scroll | down | page"
HCI4: "click | Deconstructed: The Lab-Leak Theory Is Looking Stronger | result 7 title"
HCI5: "scroll | down | page"
HCI6: "click | Unclassified-Summary-of-Assessment-on-COVID-19-Origins.pdf | result 16 title"
HCI7: "scroll | down | page"
HCI8: "click | News: The Covid-19 Lab Leak Theory Is a Tale ... (WIRED) | result 18 title"

[[pid: patch_007_11]]
--+-{ anchor_007_13 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"
HCI6: "click | Read more | hyperlink"

[[pid: patch_007_12]]
--+-{ anchor_007_14 }-+--
HCI1: "scroll | down | page"
HCI2: "click | close | pop-up"
HCI3: "scroll | down | page"
HCI4: "hover | strongly suggests | hyperlink"
HCI5: "scroll | down | page"

[[pid: patch_007_13]]
--+-{ anchor_007_15 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_007_14]]
--+-{ anchor_007_16 }-+--
HCI1: "click | close | pop-up"
HCI2: "scroll | down | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
