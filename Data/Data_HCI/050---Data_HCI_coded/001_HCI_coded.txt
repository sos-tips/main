[[sourceId=source001]]
[[streamId=HCI]]
[[cid=case001]]

[[pid: patch_001_1]]
--+-{ anchor_001_1 }-+--
HCI1: "type | scholar.google.com | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_1]]
--+-{ anchor_001_2 }-+--
HCI1: "type | sjr | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_2]]
--+-{ anchor_001_3 }-+--
HCI1: "click | Scimago Journal & Country Rank | result 1 title"

[[pid: patch_001_3]]
--+-{ anchor_001_4 }-+--
HCI1: "type | ('COVID-19' OR 'novel coronavirus' AND 'Wuhan' OR 'Origin') | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_4]]
--+-{ anchor_001_5 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "type | ('COVID-19' OR 'novel coronavirus' AND 'Wuhan' OR 'Origin' AND ('bat')) | search bar"
HCI6: "type | enter | search bar"

[[pid: patch_001_5]]
--+-{ anchor_001_6 }-+--
HCI1: "scroll | down | page"
HCI2: "click | The origin of COVID-19 and why it matters | result 7 DirectLink"

[[pid: patch_001_6]]
--+-{ anchor_001_7 }-+--

[[pid: patch_001_7]]
--+-{ anchor_001_8 }-+--
HCI1: "type | american journal tropical medicine | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_8]]
--+-{ anchor_001_9 }-+--
HCI1: "click | American Journal of Tropical Medicine and Hygiene | result 1 title"

[[pid: patch_001_9]]
--+-{ anchor_001_10 }-+--
HCI1: "scroll | down | page"

[[pid: patch_001_6]]
--+-{ anchor_001_11 }-+--
HCI1: "scroll | down | page"

[[pid: patch_001_1]]
--+-{ anchor_001_12 }-+--
HCI1: "type | jspaint.app | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_6]]
--+-{ anchor_001_13 }-+--
HCI1: "highlight | screenshot | page"
HCI2: "type | Ctrl + C | page"

[[pid: patch_001_10]]
--+-{ anchor_001_14 }-+--
HCI1: "type | Ctrl + V | page"
HCI2: "click | screenshot | page"

[[pid: patch_001_6]]
--+-{ anchor_001_15 }-+--
HCI1: "scroll | down | page"

[[pid: patch_001_5]]
--+-{ anchor_001_16 }-+--
HCI1: "click | Cited by | result 7 title"

[[pid: patch_001_11]]
--+-{ anchor_001_17 }-+--
HCI1: "click | Emerging pandemic diseases: how we got to COVID-19 | result 1 DirectLink"
HCI2: "scroll | down | page"
HCI3: "click | SARS-CoV-2, Covid-19, and the debunking of conspiracy theories | result 6 DirectLink"
HCI4: "scroll | down | page"

[[pid: patch_001_12]]
--+-{ anchor_001_18 }-+--
HCI1: "scroll | down | page"

[[pid: patch_001_13]]
--+-{ anchor_001_19 }-+--
HCI1: "click | zoom in | page"
HCI2: "scroll | down | page"
HCI3: "scroll | up | page"
HCI4: "scroll | down | page"
HCI5: "scroll | up | page"
HCI6: "scroll | down | page"
HCI7: "click | http://doi.org/10.1002/rmw.2222 | hyperlink" [[HCI>APPR>DOI]]

[[pid: patch_001_14]]
--+-{ anchor_001_20 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_001_9]]
--+-{ anchor_001_21 }-+--
HCI1: "type | reviews medical virology | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_001_9]]
--+-{ anchor_001_22 }-+--
HCI1: "click | Reviews in Medical Virology | result 1 title"

[[pid: patch_001_8]]
--+-{ anchor_001_23 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

[[pid: patch_001_14]]
--+-{ anchor_001_24 }-+--
HCI1: "scroll | down | page"