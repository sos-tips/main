[[sourceId=source015]]
[[streamId=HCI]]
[[cid=case015]]

[[pid: patch_015_1]]
--+-{ anchor_015_1 }-+--
HCI1: "type | covid origin | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_015_2]]
--+-{ anchor_015_2 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Scientists 'strongly condemn' rumors and conspiracy theories... | result 1 title"

[[pid: patch_015_3]]
--+-{ anchor_015_3 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"

[[pid: patch_015_1]]
--+-{ anchor_015_4 }-+--
HCI1: "type | assail | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_015_4]]
--+-{ anchor_015_5 }-+--

[[pid: patch_015_3]]
--+-{ anchor_015_6 }-+--
HCI1: "scroll | down | page"

[[pid: patch_015_2]]
--+-{ anchor_015_7 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Covid origin studies say evidence points to Wuhan market - BBC | result 4 title"

[[pid: patch_015_5]]
--+-{ anchor_015_8 }-+--
HCI1: "scroll | down | page"
HCI2: "click | close | pop-up"
HCI3: "scroll | down | page"
HCI4: "highlight | But knowing what we know about the virus now, it's exactly what we would expect - because many people only get very mildly ill, so they would be out in the community transmitting the virus to others and the severe cases would be hard to link to each other. | page"
HCI5: "highlight | The Covid-19 case-mapping research found that a large percentage of early patients - with no known connection to the market, meaning they neither worked or shopped there - did turn out to live near it. | page"
HCI6: "scroll | down | page"
HCI7: "highlight | But that hypothesis said Prof Stuart Neil from Kings College, can't explain the data. | page"
HCI8: "scroll | down | page"
HCI9: "highlight | But that hypothesis, said Prof Stuart Neil from Kings College, 'can't explain the data'. 'We're now as sure as we can be, based on the fragmentary evidence we do have, that this was a spillover event that happened in the market.' Crowded, live animal markets, many scientists agree, provide an ideal transmission hotspot for new diseases to 'spill over' from animals. And between 2017 and 2019, to the beginning of the pandemic, a separate study showed that nearly 50,000 animals - of 38 different species - were sold at markets in Wuhan. Prof Neil said the pandemic was very likely to have been a consequence of an 'unhealthy, cruel and unhygienic practice that Chinese authorities had been warned about'. | page"
HCI10: "scroll | down | page"

[[pid: patch_015_1]]
--+-{ anchor_015_9 }-+--
HCI1: "type | where did covid come from | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_015_6]]
--+-{ anchor_015_10 }-+--
HCI1: "click | Where did COVID-19 come from? - Boston University | result 1 title"

[[pid: patch_015_7]]
--+-{ anchor_015_11 }-+--
HCI1: "scroll | down | page"

[[pid: patch_015_6]]
--+-{ anchor_015_12 }-+--
HCI1: "scroll | down | page"
HCI2: "click | Wuhan market say epicentre of pandemic's start, studies... | result 2 title"

[[pid: patch_015_8]]
--+-{ anchor_015_13 }-+--
HCI1: "scroll | down | page"
HCI2: "click | close | pop-up"
HCI3: "scroll | down | page"
HCI4: "highlight | . Two of the reports trace the outbreak back to a massive market that sold live animals, among other goods, in Wuhan, China1,2, and a third suggests that the coronavirus SARS-CoV-2 spilled over from animals — possibly those sold at the market — to humans at least twice in November or December 20193. P | page"
HCI5: "scroll | down | page"

[[pid: patch_015_1]]
--+-{ anchor_015_14 }-+--
HCI1: "type | did covid come from laboratory | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_015_9]]
--+-{ anchor_015_15 }-+--
HCI1: "type | covid lab leak hypothesis | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_015_10]]
--+-{ anchor_015_16 }-+--
HCI1: "click | COVID-19 lab leak theory - Wikipedia | result 3 title"

[[pid: patch_015_11]]
--+-{ anchor_015_17 }-+--
HCI1: "scroll | down | page"
HCI2: "hover | 4 | hyperlink" [[HCI>APPR>Refs]]
HCI3: "scroll | zoom in | page"
HCI4: "scroll | right | page"
HCI5: "scroll | left | page"
HCI6: "scroll | right | page"
HCI7: "scroll | zoom out | page"
HCI8: "scroll | zoom in | page"
HCI9: "scroll | right | page"
HCI10: "hover | 21 | hyperlink" [[HCI>APPR>Refs]]
HCI11: "hover | 24 | hyperlink" [[HCI>APPR>Refs]]
HCI12: "hover | 25 | hyperlink" [[HCI>APPR>Refs]]
HCI13: "hover | 22 | hyperlink" [[HCI>APPR>Refs]]
HCI14: "hover | 23 | hyperlink" [[HCI>APPR>Refs]]
HCI15: "click | 24 | hyperlink" [[HCI>APPR>Refs]]
HCI16: "click | Archived | hyperlink" [[HCI>APPR>Refs]]

[[pid: patch_015_12]]
--+-{ anchor_015_18 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "scroll | down | page"
HCI6: "highlight | he debate over the lab-leak hypothesis has been rumbling since last year. But it has grown louder in the past month — even without strong supporting evidence. | page"
HCI7: "scroll | down | page"
HCI8: "highlight | On 14 May, 18 researchers published a letter in Science1 arguing that the idea of the coronavirus SARS-CoV-2 leaking from a lab in China must be explored more deeply. | page"
HCI9: "scroll | down | page"
HCI10: "scroll | up | page"
HCI11: "scroll | down | page"
HCI12: "highlight | The investigation concluded that an animal origin was much more likely than a lab leak. | page"

[[pid: patch_015_1]]
--+-{ anchor_015_19 }-+--
HCI1: "type | covid lab leak | search bar"
HCI2: "type | enter | search bar"

[[pid: patch_015_13]]
--+-{ anchor_015_20 }-+--
HCI1: "scroll | down | page"
HCI2: "type | covid lab leak theory wikipedia | search bar"
HCI3: "type | enter | search bar"

[[pid: patch_015_13]]
--+-{ anchor_015_21 }-+--
HCI1: "click | COVID-19 lab leak theory - Wikipedia | result 1 title"

[[pid: patch_015_11]]
--+-{ anchor_015_22 }-+--
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"