patchTag: "Zoom"
domain: "zoom.us"
pid: "patch_014_1"
patchType: "page"
start: "00:00"
end: "00:09"
HCI1: "type | who origin of covif | search bar"
HCI2: "type | enter | search bar"

patchTag: "Google All"
domain: "google.com"
pid: "patch_014_2"
patchType: "SERP"
start: "00:10"
end: "00:13"
HCI1: "click | Virus origin / Origins of the SARS-CoV-2 virus | result 1 title"

patchTag: "World Health Organization"
domain: "who.int"
pid: "patch_014_3"
patchType: "page"
start: "00:14"
end: "00:32"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | Download | hyperlink"
HCI4: "scroll | up | page"

patchTag: "World Health Organization"
domain: "who.int"
pid: "patch_014_4"
patchType: "file"
start: "00:33"
end: "04:21"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "type | 16 | search bar"
HCI4: "type | enter | search bar"
HCI5: "scroll | down | page"
HCI6: "scroll | up | page"
HCI7: "type | 112 | search bar"
HCI8: "type | enter | search bar"
HCI9: "scroll | down | page"
HCI10: "scroll | up | page"
HCI11: "scroll | down | page"
HCI12: "scroll | up | page"
HCI13: "scroll | down | page"
HCI14: "scroll | up | page"
HCI15: "scroll | down | page"

patchTag: "Google All"
domain: "google.com"
pid: "patch_014_2"
patchType: "SERP"
start: "04:22"
end: "04:37"
HCI1: "scroll | down | page"
HCI2: "type | possible lab origin of covid | search bar"
HCI3: "type | enter | search bar"

patchTag: "Google All"
domain: "google.com"
pid: "patch_014_5"
patchType: "SERP"
start: "04:38"
end: "04:48"
HCI1: "scroll | down | page"
HCI1: "hover | The COVID lab-leak hypothesis: what scientists do and don't | result 2 title"
HCI2: "click | Why many scientists say it's unlikely that SARS-CoV... - Science | result 1 title"

patchTag: "Science"
domain: "science.org"
pid: "patch_014_6"
patchType: "page"
start: "04:49"
end: "10:00"
HCI1: "scroll | down | page"
HCI2: "click | close | pop-up"
HCI3: "scroll | down | page"
HCI4: "click | intelligence review | hyperlink"
HCI5: "scroll | down | page"
HCI6: "hover | six times | hyperlink"
HCI7: "scroll | down | page"
HCI8: "scroll | up | page"
HCI9: "scroll | down | page"
HCI10: "scroll | up | page"
HCI11: "scroll | down | page"
HCI12: "scroll | up | page"
HCI13: "scroll | down | page"
HCI14: "scroll | up | page"
HCI15: "scroll | down | page"
HCI16: "scroll | up | page"
HCI17: "click | PLOS Pathogens | pop-up"
HCI18: "scroll | down | page"
HCI19: "scroll | up | page"
HCI20: "scroll | down | page"
HCI21: "scroll | up | page"
HCI22: "scroll | down | page"
