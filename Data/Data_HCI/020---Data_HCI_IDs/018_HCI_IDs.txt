[[sourceId=source018]]
[[streamId=HCI]]
[[cid=case018]]

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_1"
patchType: "engine"
start: "00:00"
end: "00:42"
HCI1: "type | how do coronavisruses originate in human population | search bar"
HCI2: "type | enter | search bar"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_2"
patchType: "SERP"
start: "00:43"
end: "00:47"
HCI1: "click | Origins of Coronaviruses / NIH | result 1 title"

patchTag: "National Institute of Allergy and Infectious Diseases"
domain: "niaid.nih.gov"
pid: "patch_018_3"
patchType: "page"
start: "00:48"
end: "01:48"
HCI1: "highlight | SARS-CoV then spread from infected civets to people, while MERS-CoV spreads | page"
HCI2: "highlight | Research evidence suggests that SARS-CoV and MERS-CoV originated in bats. SARS-CoV then spread from infected civets to people, while MERS-CoV spreads from infected dromedary camels to people. To date, the origin of SARS-CoV-2 which caused the COVID-19 pandemic has not been identified. The scientific evidence thus far suggests that SARS-CoV-2 likely resulted from viral evolution in nature and jumped to people or through some unidentified animal host. Public health and scientific organizations are engaged in a continued international effort to uncover the origins of SARS-CoV-2, which is essential to preventing future pandemics. | page"
HCI3: "scroll | down | page"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_1"
patchType: "engine"
start: "01:49"
end: "01:54"
HCI1: "type | where did covid came from| search bar"
HCI2: "type | enter | search bar"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_4"
patchType: "SERP"
start: "01:55"
end: "02:11"
HCI1: "click | COVID-19 Timeline - David J. Senver CDC Museum | result 1 title"
HCI2: "scroll | down | page"
HCI3: "click | Where did the pandemic start? Anywhere but here... - Science | result 2 title"
HCI4: "scroll | down | page"
HCI5: "click | Origins of Coronaviruses / NIH | result 4 title
HCI6: "scroll | down | page"
HCI7: "click | Will the World Ever Solve the Mystery of COVID-19's Origin? | result 8 title"
HCI8: "scroll | up | page"
HCI9: "type | where did covid came from reddit| search bar"
HCI10: "type | enter | search bar"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_5"
patchType: "SERP"
start: "02:12"
end: "02:26"
HCI1: "hover | Best Wuhan Posts - Reddit | result 2 title"
HCI2: "click | Covid-19 origin: Scientists traced the outbreak to a Wuhan... | result 2 title"
HCI3: "scroll | down | page"
HCI4: "scroll | up | page"
HCI5: "click | COVID-19 did not come from the Wuhan Institute of Virology | result 3 title"
HCI6: "scroll | down | page"
HCI7: "scroll | up | page"
HCI8: "type | how did the chinese make covid| search bar"
HCI9: "type | enter | search bar"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_6"
patchType: "SERP"
start: "02:27"
end: "02:45"
HCI1: "type | china search engine | search bar"
HCI2: "type | enter | search bar"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_7"
patchType: "SERP"
start: "02:46"
end: "02:52"
HCI1: "type | baidu | search bar"
HCI2: "type | enter | search bar"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_8"
patchType: "SERP"
start: "02:53"
end: "02:56"
HCI1: "click | Chinese Website | result 1 title"

patchTag: "Baidu"
domain: "baidu.com"
pid: "patch_018_9"
patchType: "engine"
start: "02:57"
end: "03:00"
HCI1: "type | how did the chinese make covid | search bar"
HCI2: "type | enter | search bar"

patchTag: "Baidu"
domain: "baidu.com"
pid: "patch_018_10"
patchType: "SERP"
start: "03:01"
end: "03:15"
HCI1: "click | COVID-19: HOW CHINA DID IT | result 3 title"
HCI2: "scroll | down | page"
HCI3: "click | ...has valuable lessons for the world in how to fight... | result 9 title"
HCI4: "scroll | up | page"

patchTag: "National Institute of Allergy and Infectious Diseases"
domain: "niaid.nih.gov"
pid: "patch_018_3"
patchType: "page"
start: "03:16"
end: "03:19"

patchTag: "Chinese Website"
domain: "mfa.gov.cn"
pid: "patch_018_11"
patchType: "page"
start: "03:21"
end: "03:23"
HCI1: "scroll | down | page"

patchTag: "Financial Times"
domain: "ft.com"
pid: "patch_018_12"
patchType: "page"
start: "03:24"
end: "03:28"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

patchTag: "Centers for Disease Control and Prevention"
domain: "cdc.gov"
pid: "patch_018_13"
patchType: "page"
start: "03:30"
end: "04:04"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "scroll | down | page"
HCI4: "highlight | imeline provides information about select moments in the COVID-19 pandemic in the United States and around the world beginning from its known origins to today. | page"
HCI5: "scroll | down | page"
HCI6: "highlight | A cluster of patients in China’s Hubei Province, in the city of Wuhan, begin to experience the symptoms of an atypical pneumonia-like illness that does not respond well to standard treatments. | page"
HCI7: "scroll | down | page"
HCI8: "highlight | The World Health Organization (WHO) Country Office in China is informed of several cases of a pneumonia of unknown etiology (cause) with symptoms including shortness of breath and fever occurring in Wuhan, China. All initial cases seem connected to the Huanan Seafood Wholesale Market. | page"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_2"
patchType: "SERP"
start: "04:05"
end: "04:07"
HCI1: "type | how do coronavruses originate | search bar"
HCI2: "type | enter | search bar"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_14"
patchType: "SERP"
start: "04:08"
end: "04:19"
HCI1: "highlight | ma | result 1 snippet"
HCI2: "click | What Is Coronavirus? / Hohns Hopkins Medicine | result 1 title"
HCI3: "scroll | down | page"
HCI4: "click | next page | pagination"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_15"
patchType: "SERP"
start: "04:20"
end: "04:35"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "click | New studies agree that animals sold at Wuhan market... - CNN | result 15 title"
HCI4: "scroll | down | page"
HCI5: "click | Updated Assessment on COVID-19 Origins | result 17 title"

patchTag: "CNN"
domain: "cnn.com"
pid: "patch_018_16"
patchType: "SERP"
start: "04:36"
end: "05:05"
HCI1: "scroll | down | page"
HCI2: "click | posted online as preprints | hyperlink"
HCI3: "scroll | down | page"
HCI4: "highlight | lecular approach and seems to determine when the first coronavirus infections crossed from animals to humans. The earliest version of the coronavirus, this research shows, probably came in different forms that the scientists call A and B. The lineages were the result of at least two cross-species transmission events into humans. The researchers suggest that the first animal-to-human transmission probably happened around November 18, 2019, and it cam | page"
HCI5: "scroll | down | page"

patchTag: "CNN"
domain: "cnn.com"
pid: "patch_018_17"
patchType: "SERP"
start: "05:06"
end: "05:29"
HCI1: "scroll | down | page"

patchTag: "Director of National Intelligence"
domain: "dni.gov"
pid: "patch_018_18"
patchType: "file"
start: "05:30"
end: "05:44"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_15"
patchType: "SERP"
start: "05:45"
end: "05:56"
HCI1: "scroll | up | page"
HCI2: "highlight | https://www.dni.go Updated Assessment on COVID-19 Origins | result 17 title"
HCI3: "click | About this result | result 17 title"
HCI4: "highlight | dni.gov | pop-up"
HCI5: "click | right click pop-up | pop-up"
HCI6: "click | Go to dni.gov | pop-up"

patchTag: "Director of National Intelligence"
domain: "dni.gov"
pid: "patch_018_19"
patchType: "page"
start: "05:57"
end: "06:01"
HCI1: "scroll | down | page"

patchTag: "Director of National Intelligence"
domain: "dni.gov"
pid: "patch_018_18"
patchType: "file"
start: "06:02"
end: "06:48"
HCI1: "scroll | down | page"
HCI2: "highlight | he National Intelligence Council assess with low confidence that the initial SARS-CoV-2 infection was most likely caused by natural exposure to an animal infected with it or a close progenitor virus—a virus that probably would be more than 99 percent similar to SARS-CoV-2. These analysts give weight to China’s officials’ lack of foreknowledge, the numerous vectors for natural exposure, and other fact | page"
HCI3: "scroll | down | page"
HCI4: "highlight | ovide a more definitive explanation for the origin of COVID-19 unless new information allows them to determine the specific pathway for initial natural contact with an animal or to determine that a laboratory in Wuhan was handling SARS-CoV-2 or a close progenitor virus before COVID-19 emerg | page"
HCI5: "scroll | down | page"
HCI6: "highlight | he global scientific community—lacks clinical samples or a complete understanding of epidemiological data from the earliest COVID-19 cases. If we obtain information on the earliest cases that identified a location of interest or occupational exposure, it may alter our evaluation of hypoth | page"
HCI7: "scroll | down | page"
HCI8: "scroll | up | page"
HCI9: "scroll | down | page"
HCI10: "scroll | up | page"
HCI11: "highlight | Analysts Assess SARS-CoV-2 Not Genetically Engineered Most IC analysts assess with low confidence that | page"
HCI12: "scroll | up | page"

patchTag: "National Institute of Allergy and Infectious Diseases"
domain: "niaid.nih.gov"
pid: "patch_018_3"
patchType: "page"
start: "06:49"
end: "06:52"

patchTag: "Baidu"
domain: "baidu.com"
pid: "patch_018_10"
patchType: "SERP"
start: "06:53"
end: "06:55"

patchTag: "Centers for Disease Control and Prevention"
domain: "cdc.gov"
pid: "patch_018_13"
patchType: "page"
start: "06:56"
end: "06:59"
HCI1: "scroll | up | page"

patchTag: "Science"
domain: "science.org"
pid: "patch_018_20"
patchType: "page"
start: "07:00"
end: "07:49"
HCI1: "scroll | down | page"
HCI2: "highlight | acknowledged this was a surprising result. But they concluded relatives of SARS-CoV-2 are “extremely rare” in China and suggested that to pinpoint the pandemic’s roots, “extensive” bat surveys should take place abroad, in the Indochina Peninsula. | page"
HCI3: "highlight | mic’s roots, “extensive” bat surveys should take place abroad, in the Indochina Peninsula. “I don’t believe it for a second,” says Hughes, a conservat | page"
HCI4: "scroll | down | page"
HCI5: "highlight | d in Wuhan from abroad, borne by contaminated frozen food or infected foreigners—perhaps at the Military World Games in Wuhan, in October 2019—or released accidentally by a U.S. military lab located more than 12,000 kilometers from Wuhan. Its goal is to avoid being blamed for the pandemic in any way, says Filippa Lentzos, a sociologist at King’s College London who studies biological threats and health security. “China just doesn’t want to look bad,” she says. “They need to maintain an image of control and competence. And that is what goes through everything they do. Police closed the Huanan Seafood Market in Wuhan in January 2020, shortly after the first cluster of COVID-19 | page"
HCI6: "scroll | down | page"
HCI7: "highlight | e government also began to clamp down on interactions between origin researchers and the media. Suddenly, giving an interview required permission from the Ministry of Science and Technology. “Part of that was for a good reason,” Hughes says. “There was some very bad science going on, and they didn’t want that | page"
HCI8: "scroll | down | page"
HCI9: "highlight | There is no shame in admitting that the virus came from wild animals illegally sold at a market, Newman adds. “To leave it as an open question, it’s just going to breed intrigue about why the Chinese have not given us a clear explanation,” Newman says. “Why the smoke and mirrors? Whereas if they come up with something that we could all just accept, well, then it will be case closed. Can’t we persuade them that this is the right thing for everybody to do?” | page"
HCI10: "scroll | up | page"
HCI11: "highlight | Wu says many Chinese researchers remain open to the idea that the virus originated in China. “We have never ruled out Wuhan or China in the next phase of the investigation, and Chinese researchers are still working on it and continuously getting data,” he says. He stresses that, in response to peer reviewers, his team has “toned down” some of its bold assertions in the Research Square preprint about the bat survey in China. For example, instead of calling viruses related to SARS-CoV-2 in Chinese bats “extremely rare,” the manuscript now says they “might be rare.” But the clampdown on bat research that Hughes experie | page"

patchTag: "Council on Foreign Relations"
domain: "cfr.org"
pid: "patch_018_21"
patchType: "page"
start: "07:50"
end: "08:25"
HCI1: "scroll | down | page"
HCI2: "highlight | Scientists say uncovering the virus’s origin is important for better understanding the risk levels of different human behaviors and preventing the next outbreak. | page"
HCI3: "scroll | down | page"
HCI4: "click | a conspiracy theory | hyperlink"
HCI5: "scroll | down | page"
HCI6: "highlight | But recent U.S. intelligence reports on the possibility of accidental lab transmission have made it a more credible hypothesis for some. Much focus has been placed on the Wuhan Institute of Virology (WIV), the first lab in China to achieve the highest level of biocontainment, known as Biosafety Level 4 (BSL-4). (There are several dozen facilities around the world that have reached this level.) U.S. diplomats who visited the lab in 2018 raised concerns about vulnerabilities, including shortages of sufficiently trained technicians and investigators; and in 2021, U.S. intelligence reported that several WIV researchers studying bat coronaviruses became ill just before the pandemic took off in late 2019. The Wuhan Center for Disease Control and Prevention has likewise been cast into the spotlight over its work on bat coronaviruses. | page"
HCI7: "scroll | down | page"

patchTag: "Bulletin of the Atomic Scientists"
domain: "thebulletin.org"
pid: "patch_018_22"
patchType: "page"
start: "08:26"
end: "08:33"

patchTag: "Reddit"
domain: "reddit.com"
pid: "patch_018_23"
patchType: "page"
start: "08:34"
end: "08:47"
HCI1: "scroll | down | page"

patchTag: "The New Reddit Journal of Science"
domain: "reddit.com"
pid: "patch_018_24"
patchType: "page"
start: "08:48"
end: "08:50"

patchTag: "Centers for Disease Control and Prevention"
domain: "cdc.gov"
pid: "patch_018_13"
patchType: "page"
start: "08:51"
end: "08:53"
HCI1: "scroll | down | page"

patchTag: "Baidu"
domain: "baidu.com"
pid: "patch_018_10"
patchType: "SERP"
start: "08:54"
end: "09:11"
HCI1: "scroll | down | page"
HCI2: "scroll | up | page"
HCI3: "type | coronavirus wikipedia | search bar"
HCI4: "type | enter | search bar"

patchTag: "Google All"
domain: "google.com"
pid: "patch_018_25"
patchType: "SERP"
start: "09:12"
end: "09:14"
HCI1: "click | Coronavirus - Wikipedia | result 1 title"

patchTag: "Wikipedia"
domain: "wikipedia.org"
pid: "patch_018_26"
patchType: "page"
start: "09:15"
end: "10:23"
HCI1: "highlight | at ca | page"
HCI2: "scroll | down | page"
HCI3: "click | Transmission | hyperlink"
HCI4: "scroll | up | page"
HCI5: "click | Origin | hyperlink"
HCI6: "highlight | ough some models place | page"
HCI7: "highlight | most recent common ancestor of the alphacoronavirus line has been placed at about 2400 BCE, of the betacoronavirus line at 3300 BCE, of the gammacoronavirus line at 2800 BCE, and the deltacoronavirus line at about 3000 BCE. Bats and birds, as warm-blooded flying vertebrates, are an ideal natural reservoir for the coronavirus gene pool (with bats the reservoir for alphacoronaviruses and betacoronavirus – and birds the reservoir for gammacoronaviruses and deltacoronaviruses). The large number and global range of bat and avian species that host viruses have enabled extensive evolution and dissemination of coronaviruses. | page"
